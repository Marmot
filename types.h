/*
 * types.h --
 *
 *      Basic marmot types.
 *
 */

#ifndef _TYPES_H
#define _TYPES_H



#ifndef ASM

typedef long long int           int64;
typedef unsigned long long int uint64;
typedef int                     int32;
typedef unsigned int           uint32;
typedef short int               int16;
typedef unsigned short int     uint16;
typedef signed char              int8;
typedef unsigned char           uint8;

typedef uint64 PA;
typedef uint64 VA;
typedef uint64 UReg;
typedef uint8  Bool;
typedef uint64 size_t;

#define TRUE  ((Bool)1)
#define FALSE ((Bool)0)


typedef uint32 Color;
typedef uint64 Length;

typedef struct {
   uint64 x;
   uint64 y;
} __attribute__((packed)) Point;

#endif

#endif
