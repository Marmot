/*
 * window.c --
 *
 *      Implementation of the windowing interface.
 */


typedef uint64 Window;

typedef struct {
   Color bg;
   Color fg;
   uint64 width;
   uint64 height;
   uint8 *fb;
   uint64 fbWidth;
   uint64 fbHeight;
   char *title;
   unsigned int hasFocus:1;
   unsigned int drawBorder:1;
} WParam;




Window
Window_Create(WParam *wp)
{
   if (wp->bg == COLOR_DEFAULT) {
      wp->bg = COLOR_WINDOW_BACKGROUND;
   }
   if (wp->fg == COLOR_DEFAULT) {
      wp->fg = COLOR_BLACK;
   }
   
}

