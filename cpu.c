/*
 * cpu.c
 *
 *      CPU-specific functionality.
 */


#include <marmot.h>


struct {
   uint64  nStandard;
   uint64  nHypervisor;
   uint64  nExtended;
   struct {
      UReg eax;
      UReg ebx;
      UReg ecx;
      UReg edx;
   } features[];
} *CPUIDInfo;


uint32
CPU_GetFeature(uint32 feature)
{
   uint32 type, fcn, reg, bl, bh, mask, idx, features = 0;

   type = (feature & CPUID_TYPE_MASK) >> CPUID_TYPE_SHIFT;
   fcn = (feature & CPUID_FCN_MASK) >> CPUID_FCN_SHIFT;
   reg = (feature & CPUID_REG_MASK) >> CPUID_REG_SHIFT;
   bl = (feature & CPUID_LBIT_MASK) >> CPUID_LBIT_SHIFT;
   bh = (feature & CPUID_HBIT_MASK) >> CPUID_HBIT_SHIFT;

   mask = (~((1 << bh) - 1)) - ((1 << bl) - 1);

   idx = 0;
   if (type == CPUID_FEATURES_HYPERVISOR) {
      if (CPUIDInfo->nHypervisor == 0) {
         return FALSE;
      }

      idx += CPUIDInfo->nStandard;
   } else if (type == CPUID_FEATURES_EXTENDED) {
      idx += CPUIDInfo->nStandard + CPUIDInfo->nHypervisor;
   }

   switch (reg) {
   case CPUID_REGISTER_EAX:
      features = CPUIDInfo->features[idx].eax;
      break;
   case CPUID_REGISTER_EBX:
      features = CPUIDInfo->features[idx].ebx;
      break;
   case CPUID_REGISTER_ECX:
      features = CPUIDInfo->features[idx].ecx;
      break;
   case CPUID_REGISTER_EDX:
      features = CPUIDInfo->features[idx].edx;
      break;
   }

   return (features & mask) >> bl;
}
