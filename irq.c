/*
 * irq.c --
 *
 *      Generic IRQ handler.
 */

#include <marmot.h>


struct tod {
   uint8 hour;
   uint8 minute;
   uint8 second;
} __attribute__((packed));

struct tod tod;



#define KBD_REG_STATUS  0x64
#define KBD_STATUS_OUT_DATA     (1 << 0)
#define KBD_STATUS_IN_DATA      (1 << 1)
#define KBD_STATUS_SYSFLAG      (1 << 2)
#define KBD_STATUS_INDATA_CMD   (1 << 3)
#define KBD_STATUS_ENABLE       (1 << 4)
#define KBD_STATUS_XMIT_TIMEOUT (1 << 5)
#define KBD_STATUS_AUX_DATA     (1 << 5)
#define KBD_STATUS_RECV_TIMEOUT (1 << 6)
#define KBD_STATUS_PARITY_EVEN  (1 << 7)

#define KBD_CMD_KBDIRQ 0x01
#define KBD_CMD_AUXIRQ 0x02
#define KBD_CMD_KBDDIS 0x10
#define KBD_CMD_AUXDIS 0x20
#define KBD_CMD_XLATE  0x40


#define KBD_REG_COMMAND 0x64
#define KBD_REG_DATA    0x60

#define MOUSE_ACK 0xfa


#define RTC_PORT_ADDR  0x70
#define RTC_PORT_DATA  0x71

#define RTC_SEC        0x00
#define RTC_ASEC       0x01
#define RTC_MIN        0x02
#define RTC_AMIN       0x03
#define RTC_HOUR       0x04
#define RTC_AHOUR      0x05
#define RTC_DOW        0x06
#define RTC_DOM        0x07
#define RTC_MON        0x08
#define RTC_YEAR       0x09

#define RTC_REG_A      0x0a
#define RTC_REG_A_RATE 0x0f
#define RTC_REG_A_DIV  0x70
#define RTC_REG_A_UIP  0x80

#define RTC_REG_B      0x0b
#define RTC_REG_B_DST  (1 << 0)
#define RTC_REG_B_24H  (1 << 1)
#define RTC_REG_B_BIN  (1 << 2)
#define RTC_REG_B_SQE  (1 << 3)
#define RTC_REG_B_UIE  (1 << 4)
#define RTC_REG_B_AIE  (1 << 5)
#define RTC_REG_B_PIE  (1 << 6)
#define RTC_REG_B_DCU  (1 << 7)

#define RTC_REG_C      0x0c
#define RTC_REG_C_UIE  (1 << 4)
#define RTC_REG_C_AIE  (1 << 5)
#define RTC_REG_C_PIE  (1 << 6)
#define RTC_REG_C_IRQF (1 << 7)

#define RTC_REG_D
#define RTC_REG_D_RAMP (1 << 7)

#define BCD_TO_BIN(b) (((((b) >> 4) & 0xf) * 10) + ((b) & 0xf))


void
MaskPIC(uint8 irq)
{
   uint8 mask, port;

   if (irq > 7) {
      irq -= 8;
      port = 0xa1;
   } else {
      port = 0x21;
   }

   mask = inb(port);
   mask |= (1 << irq);
   outb(mask, port);
}

void
UnmaskPIC(uint8 irq)
{
   uint8 mask;

   if (irq > 7) {
      mask = inb(0xa1);
      mask &= ~(1 << (irq - 8));
      outb(mask, 0xa1);

      irq = 2;
   }

   mask = inb(0x21);
   mask &= ~(1 << irq);
   outb(mask, 0x21);
}

void
AckPIC(uint8 irq)
{
   if (irq > 7) {
      outb(0x60 | (irq - 8), 0xa0);
      irq = 2;
   }

   outb(0x60 | irq, 0x20);
}




CbID
GetCBId(void)
{
   static uint64 curID = 0;

   /*
    * XXX: One day this should be a little more robust than a one-up.
    */

   return ++curID;
}

static struct MouseCB *mouseCB;

CbID
InstallMouseCB(MouseCBFun cb)
{
   struct MouseCB *new;

   new = alloc(sizeof(struct MouseCB));
   new->next = mouseCB;
   new->id = GetCBId();
   new->cb = cb;

   mouseCB = new;

   return new->id;
}

void
RemoveMouseCB(CbID id)
{
   struct MouseCB *s, *del = NULL;

   ASSERT(mouseCB != NULL);

   /* Shouldn't need a lock here */
   if (mouseCB->id == id) {
      s = mouseCB;
      mouseCB = s->next;
      del = s;
   } else {
      for (s = mouseCB; s->next != NULL; s = s->next) {
         if (s->next->id == id) {
            del = s->next;
            s->next = s->next->next;
            break;
         }
      }
   }

   ASSERT(del != NULL);
   bzero(del, sizeof(struct MouseCB));
   free(del);
}


void
InstallIDTEntry(uint64 vec, void (*stub)(void))
{
   uint64 *idt = (uint64 *)IDT;
   VA stubAddr = (VA)stub;

   *(idt + 2 * vec) = (((stubAddr & 0xffff0000) << 32) |
                       0x8e0100000000 |
                       ((CS64 & 0xffff) << 16) |
                       (stubAddr & 0xffff));
   *(idt + 2 * vec + 1) = (stubAddr >> 32) & 0xffffffff;
}


void
DisplayClock(void)
{
   Point p0, p1;
   char time[] = { ' ', ' ', ':', ' ', ' ', ':', ' ', ' ', 0 };

   time[0] = tod.hour / 10 + '0';
   time[1] = tod.hour % 10 + '0';
   time[3] = tod.minute / 10 + '0';
   time[4] = tod.minute % 10 + '0';
   time[6] = tod.second / 10 + '0';
   time[7] = tod.second % 10 + '0';

   p1.x = xResolution - 4;
   p1.y = yResolution - 4;

   p0.x = p1.x - 8 * 8;
   p0.y = p1.y - 16;

   ColorRectangle(COLOR_PLEASING_GREEN, p0, p1);
   PrintMessage(COLOR_BLACK, p0, time);
}

void
UpdateClock(void)
{
   /* ACK the interrupt */
   outb(RTC_REG_C, RTC_PORT_ADDR);
   inb(RTC_PORT_DATA);

   /* update the time of day */
   if (++tod.second >= 60) {
      tod.second = 0;
      if (++tod.minute >= 60) {
         tod.minute = 0;
         if (++tod.hour >= 24) {
            tod.hour = 0;
         }
      }
   }
}


static inline void
KbdWaitWrite(void)
{
   while (inb(KBD_REG_STATUS) & KBD_STATUS_IN_DATA); // 0x2
}

static inline void
KbdWaitRead(void)
{
   while (~inb(KBD_REG_STATUS) & KBD_STATUS_OUT_DATA); // 0x1
}

uint8
ReadKeyboard(void)
{
   KbdWaitRead();
   return inb(KBD_REG_DATA);
}


void
ClearKeyDisplay(void)
{
   Point p0, p1;

   p0.x = 4;
   p0.y = yResolution - 4 - 16;

   p1.x = 4 + 8 * 16; // max of 16 16 pix wide chars
   p1.y = yResolution - 4;

   ColorRectangle(COLOR_PLEASING_GREEN, p0, p1);
}

char keys[8];
uint64 keyp;

#define lbHex(b) ((((b) & 0xf) < 0xa) ? \
                  ((b) & 0xf) + '0' : \
                  (((b) & 0xf) - 0xa) + 'a')
#define hbHex(b) (lbHex((b) >> 4))

void
DisplayKeycode(void)
{
   Point p;
   char c[17];
   uint64 k;

   bzero(c, 17);

   for (k = 0; k < keyp; k++) {
      c[2 * k] = hbHex(keys[k]);
      c[2 * k + 1] = lbHex(keys[k]);
   }

   p.x = 4;
   p.y = yResolution - 4 - 16;

   PrintMessage(COLOR_BLACK, p, c);
}


/*
 * HaveCompleteScancode --
 *
 *      Check that a fully formed mode 2 raw scancode has been received.
 */

static inline int
HaveCompleteScancode(void)
{
   /*
    * ..
    * f0 ..
    * e0 ..
    * e0 f0 ..
    * e1 .. ..
    * e1 f0 .. f0 ..
    */

   if (keys[0] == 0xe0) {
      if (keyp == 1 || (keyp == 2 && keys[1] == 0xf0)) {
         return 0;
      }
   } else if (keys[0] == 0xe1) {
      if (keyp < 3 || (keyp == 3 && keys[1] == 0xf0) || keyp == 4) {
         return 0;
      }
   } else if (keys[0] == 0xf0 && keyp == 1) {
      return 0;
   }

   return 1;
}

/*
 * Keyboard --
 *
 *      Handle the keyboard interrupt.
 */

void
Keyboard(uint8 data)
{
   keys[keyp++] = data;

   if (HaveCompleteScancode()) {
      ClearKeyDisplay();
      DisplayKeycode();

      bzero(keys, sizeof(keys));
      keyp = 0;
   }
}


uint8 mouse[8];
uint64 mousep;
uint64 mousePacketLength;


void
MousePacketDisplay(uint8 *packet, uint64 N)
{
   Point p0, p1;
   char s[17];
   uint64 k;

   bzero(s, sizeof(s));

   p0.x = 4;
   p0.y = yResolution - 8 - 32;
   p1.x = 4 + 8 * 8;
   p1.y = p0.y + 16;
   ColorRectangle(COLOR_PLEASING_GREEN, p0, p1);

   for (k = 0; k < N; k++) {
      s[2 * k] = hbHex(packet[k]);
      s[2 * k + 1] = lbHex(packet[k]);
   }

   PrintMessage(COLOR_BLACK, p0, s);
}


void
Mouse(uint8 data)
{
   mouse[mousep++] = data;

   if (mousep == mousePacketLength) {
      struct MouseCB *s;

      for (s = mouseCB; s != NULL; s = s->next) {
         s->cb(mouse, mousep);
      }

      bzero(mouse, sizeof(mouse));
      mousep = 0;
   }
}


void
i8042(void)
{
   uint8 status, data;

   status = inb(KBD_REG_STATUS);
   if (!(status & KBD_STATUS_OUT_DATA)) {
      return;
   }

   data = inb(KBD_REG_DATA);

   if (status & KBD_STATUS_AUX_DATA) {
      Mouse(data);
   } else {
      Keyboard(data);
   }
}


/*
 * _IRQPrint --
 *
 *      Display an indicator of received interrupts.
 */

static void
_IRQPrint(uint64 irq)
{
   /* Not reentrant, but that isn't important here. */
   static int counts[] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
   char s[] = { 'I', 'R', 'Q', ' ', '(', ' ', ')', 0 };
   char c[] = { '-', '\\', '|', '/' };
   int count = 0;
   Point p0, p1;

   if (count < 16) {
      count = counts[irq]++;
   } else {
      count = counts[16]++;
   }

   s[3] = (irq < 0xa) ? irq + 0x30 : irq - 0xa + 'a';
   s[5] = c[count % sizeof(c)];

   p0.x = 10;

   if (irq <= 0xf) {
      p0.y = 30 + irq * 18;
   } else {
      p0.y = 30 + 16 * 18;
   }

   p1.x = p0.x + 8 * 8;
   p1.y = p0.y + 16;

   ColorRectangle(COLOR_PLEASING_GREEN, p0, p1);
   PrintMessage(COLOR_BLACK, p0, s);
}


void
DoIRQ(uint64 irq)
{
   _IRQPrint(irq - 0x20);

   MaskPIC(irq - 0x20);
   AckPIC(irq - 0x20);

   switch (irq) {
   case PIT_IRQ:
      TaskSchedule(irq); // does not return
      break;
   case KBD_IRQ:
   case MOUSE_IRQ:
      i8042();
      break;
   case RTC_IRQ:
      UpdateClock();
      DisplayClock();
      break;
   }

   UnmaskPIC(irq - 0x20);
}


#define PIT_C0 0x40
#define PIT_C1 0x41
#define PIT_C2 0x42
#define PIT_CW 0x43

/*
 * Actually 1193181.666... but this isn't used for keeping time to the
 * round off error isn't significant (and it's doubtful the clock is
 * that accurate anyways).
 */
#define PIT_CLK_FREQ     1193182
#define PIT_COUNTDOWN    (PIT_CLK_FREQ / TICK_Hz)

#define PIT_CW_BIN       0x00
#define PIT_CW_BCD       0x01
#define PIT_CW_MODE0     0x00
#define PIT_CW_MODE1     0x02
#define PIT_CW_MODE2     0x04
#define PIT_CW_MODE3     0x06
#define PIT_CW_MODE4     0x08
#define PIT_CW_MODE5     0x0a
#define PIT_CW_RW_LATCH  0x00
#define PIT_CW_RW_LSB    0x10
#define PIT_CW_RW_MSB    0x20
#define PIT_CW_RW_LSBMSB 0x30
#define PIT_CW_CTR0      0x00
#define PIT_CW_CTR1      0x40
#define PIT_CW_CTR2      0x80
#define PIT_CW_READBACK  0xc0


void
StartPIT(void)
{
   uint16 tick;

   /*
    * Set the PIT to TICK_Hz.
    */

   tick = PIT_COUNTDOWN;
   outb(PIT_CW_BIN | PIT_CW_MODE2 | PIT_CW_RW_LSBMSB | PIT_CW_CTR0, PIT_CW);
   outb(tick & 0xff, PIT_C0);
   outb(tick >> 8, PIT_C0);

   InstallIDTEntry(PIT_IRQ, _IRQStub_PIT);
   UnmaskPIC(PIT_IRQ - 0x20);
}


void
StartClock(void)
{
   /*
    * Disable daylight savings, enable 24 hour clock, and enable
    * update ended interrupt.
    */

   outb(RTC_REG_B, RTC_PORT_ADDR);
   outb(RTC_REG_B_24H | RTC_REG_B_UIE, RTC_PORT_DATA);

   /*
    * Initialize the clock.
    */

   do {
      outb(RTC_REG_A, RTC_PORT_ADDR);
      if ((inb(RTC_PORT_DATA) & RTC_REG_A_UIP) == 0) {
         break;
      }
   } while (1);

   outb(RTC_SEC, RTC_PORT_ADDR);
   tod.second = BCD_TO_BIN(inb(RTC_PORT_DATA));
   outb(RTC_MIN, RTC_PORT_ADDR);
   tod.minute = BCD_TO_BIN(inb(RTC_PORT_DATA));
   outb(RTC_HOUR, RTC_PORT_ADDR);
   tod.hour = BCD_TO_BIN(inb(RTC_PORT_DATA));

   DisplayClock();

   /* enable in PIC */
   InstallIDTEntry(RTC_IRQ, _IRQStub_RTC);
   UnmaskPIC(RTC_IRQ - 0x20);
}


void
i8042_WriteCmd(uint8 cmd)
{
   KbdWaitWrite();
   outb(cmd, KBD_REG_COMMAND);
}

uint8
i8042_ReadData(void)
{
   //KbdWaitRead();
   return inb(KBD_REG_DATA);
}

void
i8042_WriteData(uint8 data)
{
   KbdWaitWrite();
   outb(data, KBD_REG_DATA);
}

void
KeyboardInit(void)
{
   uint8 cmd;

   /* Get keyboard command register. */
   i8042_WriteCmd(0x20);
   cmd = i8042_ReadData();

   /* Enable keyboard. */
   cmd &= ~KBD_CMD_KBDDIS;
   cmd |= KBD_CMD_KBDIRQ;

   i8042_WriteCmd(0x60);
   i8042_WriteData(cmd);

   /* Set keyboard mode 2. */
   i8042_WriteData(0xf0);
   i8042_WriteData(0x02);

   /* Clear keyboard buffer. */

   bzero(keys, sizeof(keys));
   keyp = 0;

   /* Install handler. */
   InstallIDTEntry(KBD_IRQ, _IRQStub_KBD);
   UnmaskPIC(KBD_IRQ - 0x20);
}


uint8
i8042_WriteAuxCmd(uint8 cmd)
{
   KbdWaitWrite();
   outb(0xd4, KBD_REG_COMMAND);
   KbdWaitWrite();
   outb(cmd, KBD_REG_DATA);

   return inb(KBD_REG_DATA);
}

void
MouseInit(void)
{
   uint8 cmd, data;

   /* Get keyboard command register. */
   i8042_WriteCmd(0x20);
   cmd = i8042_ReadData();

   /* Enable PS/2 mouse. */
   cmd &= ~KBD_CMD_KBDDIS;
   cmd |= KBD_CMD_KBDIRQ;

   i8042_WriteCmd(0x60);
   i8042_WriteData(cmd);

   i8042_WriteData(0xa8);

   /* Check for intellimouse extension */
   i8042_WriteAuxCmd(0xf3);
   i8042_WriteAuxCmd(200);
   i8042_WriteAuxCmd(0xf3);
   i8042_WriteAuxCmd(100);
   i8042_WriteAuxCmd(0xf3);
   i8042_WriteAuxCmd(80);

   i8042_WriteAuxCmd(0xf2);
   data = inb(KBD_REG_DATA);
   
   if (data == 0x03) {
      mousePacketLength = 4;
   } else {
      mousePacketLength = 3;
   }

   /* Set sample rate */
   i8042_WriteAuxCmd(0xf3);
   i8042_WriteAuxCmd(MOUSE_SAMPLE_RATE);

   /* Set stream mode */
   i8042_WriteAuxCmd(0xea); // set stream mode

   /* Set resolution */
   i8042_WriteAuxCmd(0xe8);
   i8042_WriteAuxCmd(3);
   
   /* set scaling */
   i8042_WriteAuxCmd(0xe7);

   /* Enable data reporting */
   i8042_WriteAuxCmd(0xf4); // enable data reporting

   bzero(mouse, sizeof(mouse));
   mousep = 0;

   mouseCB = NULL;

   InstallMouseCB(MousePacketDisplay);

   /* Install mouse interrupt handler. */
   InstallIDTEntry(MOUSE_IRQ, _IRQStub_MOUSE);
   UnmaskPIC(MOUSE_IRQ - 0x20);
}
