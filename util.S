        /*
         * util.S --
         *
         *      Various generic utility functions.
         */


        .code64
        .section .text

        /*
         * bzero --
         * bfill --
         *
         *      void bzero(void *s, uint64 n);
         *      void bfill(void *s, uint64 n, uint8 val);
         *
         *      Set the first n bytes of s to 0 or to val.
         */

        .global bzero
bzero:  pushq   %rbp
        movq    %rsp, %rbp

        xorq    %rdx, %rdx
        jmp     1f


        .global bfill
bfill:  pushq   %rbp
        movq    %rsp, %rbp

        /* copy %dl to each byte of %rdx */
        movzbq  %dl, %rdx
        movq    %rdx, %rax
        clc
        shll    $8, %edx
        orl     %eax, %edx
        /* 2 bytes done */
        movl    %edx, %eax
        shll    $16, %edx
        orl     %eax, %edx
        /* 4 bytes done */
        movq    %rdx, %rax
        shlq    $32, %rdx
        orq     %rax, %rdx
        /* all 8 done */

        /* set bytes while adjusting s up to 8 byte boundary */
1:      testl   $7, %esi
        jz      1f

        movb    %dl, (%rdi)
        decq    %rsi
        incq    %rdi
        jmp     1b

1:      /* zero bytes 8 at a time */
        
        testq   $-8, %rsi
        jz      1f

        movq    %rdx, (%rdi)
        subq    $8, %rsi
	addq    $8, %rdi
        jmp     1b

1:      /* zero out the remainder */
        testl   $7, %esi
        jz      1f

        movb    %dl, (%rdi)
        decq    %rsi
        incq    %rdi
        jmp     1b

1:      leave
        ret


        /*
         * bcopy --
         *
         *      void bcopy(void *s, void *d, uint64 n);
         *
         *      Copy n bytes from s to d.
         */

        .global bcopy
bcopy:  pushq   %rbp
        movq    %rsp, %rbp

        cld
        xchgq   %rdi, %rsi
        movq    %rdx, %rcx
        rep movsb

        leave
        ret


        /*
         * strlen --
         *
         *      Return length of a string.
         *
         *      uint64 strlen(char *s);
         */
        .global strlen
strlen: pushq   %rbp
        movq    %rsp, %rbp

        pushq   %rdi
        xorl    %eax, %eax
        cld
        repne scasb
        popq    %rax
        subq    %rax, %rdi      # d - d - s
        movq    %rdi, %rax

        leave
        ret
