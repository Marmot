/*
 * cpu.h --
 *
 *      All the various CPU defines are placed here to avoid
 *      cluttering marmot.h too much.
 */

#ifndef _CPU_H
#define _CPU_H

#include <types.h>

#define APIC_BASE_MSR 0x1b


/*
 * Note about hypervisor features:
 *
 * If cpuid[1].ecx[31] is set, then a hypervisor is present and the
 * hypervisor functions (0x400000xx) can be queried (which isn't to
 * say a hypervisor *isn't* present if the bit isn't set).
 *
 * VMware defines the following leaves:
 *
 *      0x40000000.eax: Max cpuid input value
 *                .ebx: 0x61774d56 (awMV)
 *                .ecx: 0x4d566572 (MVer)
 *                .edx: 0x65726177 (eraw)
 *
 *      0x4000000a.eax: Virtual TSC frequency in kHz
 *                .ebx: Virtual bus (local APIC timer) frequency in kHz
 *                .ecx: Reserved
 *                .edx: Reserved
 *
 *
 * Microsoft defines the following leaves:
 *
 *      0x40000000.eax: Max cpuid input value
 *                .ebx: 0x7263694d (rciM)
 *                .ecx: 0x666f736f (foso)
 *                .edx: 0x76482074 (vH t)
 *
 *      0x40000001.eax: 0x31237648 (1#vH)
 *                .ebx: Reserved
 *                .ecx: Reserved
 *                .edx: Reserved
 *
 *      0x40000002.eax: Build number
 *                .ebx: Bits 31-16: Major version
 *                      Bits 15-0:  Minor version
 *                .ecx: Service pack
 *                .edx: Bits 31-24: Service branch
 *                      Bits 23-0:  Service number
 *
 *      0x40000003.eax: Bit 0: VP runtime (HV_X64_MSR_VP_RUNTIME) is available.
 *                      Bit 1: Partition reference counter
 *                             (HV_X64_MSR_TIME_REF_COUNT) is available.
 *                      Bit 2: Basic SynIC MSRs (HV_X64_MSR_SCONTROL through
 *                             HV_X64_MSR_EOM and HV_X64_MSR_SINT0 through
 *                             HV_X64_MSR_SINT15) are available.
 *                      Bit 3: Synthetic timer MSRs (HV_X64_STIMER0_CONFIG
 *                             through HV_X64_STIMER3_COUNT) are available.
 *                      Bit 4: APIC access MSRs (HV_X64_MSR_EOI,
 *                             HV_X64_MSR_ICR, and HV_X64_MSR_TPR) are
 *                             available.
 *                      Bit 5: Hypercall MSRs (HV_X64_MSR_GUEST_OS_ID and
 *                             HV_X64_MSR_HYPERCALL) are available.
 *                      Bit 6: Access virtual process index MSR
 *                             (HV_X64_MSR_VP_INDEX) is available.
 *                      Bit 7: Virtual system reset MSR (HV_X64_MSR_RESET) is
 *                             available.
 *                      Bits 8-31: Reserved
 *                .ebx: Flags that parent partition specified to create child
 *                      partition (HV_PARTITION_PRIVILEGE_MASK):
 *                      Bit 0:  CreatePartitions
 *                      Bit 1:  AccessPartitionId
 *                      Bit 2:  AccessMemoryPool
 *                      Bit 3:  AdjustMessageBuffers
 *                      Bit 4:  PostMessages
 *                      Bit 5:  SignalEvents
 *                      Bit 6:  CreatePort
 *                      Bit 7:  ConnectPort
 *                      Bit 8:  AccessStats
 *                      Bit 9:  IteratePhysicalHardware
 *                      Bit 10: DeprecatedExposeHyperthreads
 *                      Bit 11: Debugging
 *                      Bit 12: CpuPowerManagement
 *                      Bits 13-31: Reserved
 *                .ecx: Power management information:
 *                      Bits 0-3: Maximum processor power state:
 *                                0 = C0, 1 = C1, 2 = C2, 3 = C3
 *                      Bits 4-31: Reserved
 *                .edx: Miscellaneous features available to partition:
 *                      Bit 0: MWAIT is available.
 *                      Bit 1: Guest debugging support is available.
 *                      Bit 2: Performance monitor support is available.
 *                      Bits 3-31: Reserved
 *
 *      0x40000004: Hypervisor recommendations to guest.
 *                .eax: Bit 0: Use hypercall for address space switch instead
 *                             of mov to %cr3.
 *                      Bit 1: Use hypercall for TLB flush instead of invlpg
 *                             or mov to %cr3.
 *                      Bit 2: User hypercall for remote TLB flush instead of
 *                             IPI.
 *                      Bit 3: Use MSRs to access APIC registers EOI, ICR, and
 *                             TPR instead of memory-mapped APIC registers.
 *                      Bit 4: User hypervisor MSR to initiate system RESET.
 *                      Bit 5: Use "relaxed timing" in this partition.
 *                      Bit 6-31: Reserved
 *                .ebx: Recommended spinlock retries before notifying
 *                      hypervisor (0xffffffff indicates never retry).
 *                .ecx: Reserved
 *                .edx: Reserved
 *
 *      0x40000005.eax: Maximum number of virtual processors supported.
 *                .ebx: Maximum number of physical processors supported.
 *                .ecx: Reserved
 *                .edx: Reserved
 *
 */


#define CPUID_FEATURES_STANDARD   0
#define CPUID_FEATURES_HYPERVISOR 4
#define CPUID_FEATURES_EXTENDED   8

#define CPUID_REGISTER_EAX        0
#define CPUID_REGISTER_EBX        1
#define CPUID_REGISTER_ECX        2
#define CPUID_REGISTER_EDX        3

/*
 * The feature mask for each feature looks like:
 *
 *     0000 0000 0000 tttt ffff rrll lllh hhhh
 *
 *     Type is 0, 4, or 8 (standard, hypervisor, or extended).
 *     Function is the function number within the type.
 *     Register is eax, ebx, ecx, or edx.
 *     The feature returned extends from the low bit to the high bit.
 */


#define CPUID_TYPE_SHIFT  16
#define CPUID_TYPE_MASK   0x000f0000
#define CPUID_FCN_SHIFT   12
#define CPUID_FCN_MASK    0x0000f000
#define CPUID_REG_SHIFT   10
#define CPUID_REG_MASK    0x00000c00
#define CPUID_LBIT_SHIFT  5
#define CPUID_LBIT_MASK   0x000003e0
#define CPUID_HBIT_SHIFT  0
#define CPUID_HBIT_MASK   0x0000001f

#define CPUID_FEATURE(type,fcn,reg,bl,bh) (((type) << CPUID_TYPE_SHIFT) | \
                                          ((fcn) << CPUID_FCN_SHIFT)    | \
                                          ((reg) << CPUID_REG_SHIFT)    | \
                                          ((bl) << CPUID_LBIT_SHIFT)    | \
                                          ((bh) << CPUID_HBIT_SHIFT))

#define CPU_HYPERVISOR_PRESENT CPUID_FEATURE(CPUID_FEATURES_STANDARD, \
                                             1,                       \
                                             CPUID_REGISTER_ECX,      \
                                             31,                      \
                                             31)

#define CPU_APIC_ENABLED CPUID_FEATURE(CPUID_FEATURES_STANDARD, \
                                       1,                       \
                                       CPUID_REGISTER_EDX,      \
                                       9,                       \
                                       9)

#define CPU_VMW_VTSC_KHZ CPUID_FEATURE(CPUID_FEATURES_HYPERVISOR, \
                                       0xa,                       \
                                       CPUID_REGISTER_EAX,        \
                                       0,                         \
                                       31)

/*
 * Check whether the indicated feature bit is set.
 */
uint32 CPU_GetFeature(uint32 feature);


#endif
