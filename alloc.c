/*
 * alloc.c --
 *
 *     Allocate memory of arbitrary sizes.
 *
 *     The mechanism of allocation used here is that large, CHUNK_SIZE
 *     sized blocks of memory are requested from the system.  They are
 *     kept track of by overlaying the first sizeof(struct SysBlock)
 *     bytes with a struct SysBlock and having the next pointer point
 *     to the next block, or being NULL.  That way, when Free is
 *     called, the linked list, kept in the variable memory, is walked
 *     and each node is free'd.  Each block is then carved up into
 *     smaller blocks.  Each of these smaller blocks has the first
 *     sizeof(MemHeader) bytes overlaid by a struct MemHeader.
 *     Then, if one of the smaller blocks is needed, the struct
 *     MemHeader is placed on the alloc list.  If, relative to the
 *     size of the memory request, it is fairly large, it is
 *     partitioned into two blocks.  One is placed on the alloc list
 *     and returned to the caller; the other is placed on the free
 *     list.  Then, as space is needed, adjacent free blocks are
 *     merged to create larger blocks.
 */


#include <marmot.h>


/* the default amount of memory to request from the system each time */
#define DEFAULT_CHUNK_SIZE PAGE_SIZE

/* round an amount of memory to the next page size up */
#define ROUND_TO_PAGE_SIZE(a) (((a) + (PAGE_SIZE - 1)) & ~(PAGE_SIZE - 1))

/* the minimum size of housekeeping info in a sys block */
#define ALLOC_ADJUST (sizeof(struct SysBlock) + sizeof(struct MemHeader))

/* the smallest amount which will be returned */
#define MIN_ALLOC_AMOUNT 8

/* the largest amount that can be handled at one time */
#define MAX_ALLOC_AMOUNT (chunkSize - ALLOC_ADJUST)


/* header for a block of memory - this is what is referenced on
   both the alloc and free lists */
struct MemHeader {
   /* size of the chunk, not including the header */
   uint64 size;
   struct MemHeader *next;
} __attribute__((packed));


/* header for the block returned by a call to PageAlloc */
struct SysBlock {
   struct SysBlock *next;
} __attribute__((packed));


/* the free list */
static struct MemHeader *freeList = NULL;

/* the alloc list */
static struct MemHeader *allocList = NULL;

/* the list of malloc'ed blocks */
static struct SysBlock *memory;

/* the allocation size for memory chunks */
static uint64 chunkSize;

/* whether or not memory is fragmented */
static Bool freeListNeedsCompacting = 0;

/* forward declaration needed for mem_trace */
void CompactFreeList(void);


/*
 * FreeListInsertSorted --
 *
 *      Insert NEW onto the free list, sorted numerically by address.
 */

void
FreeListInsertSorted(struct MemHeader *new)
{
   struct MemHeader *freeTmp = freeList;

   /* check for empty list */
   if (freeList == NULL) {
      freeList = new;
      freeList->next = NULL;
      return;
   }

   /* if NEW is the lowest address */
   if (new < freeTmp) {
      new->next = freeTmp;
      freeList = new;
      return;
   }

   /* general case - find the insertion point */
   while (freeTmp->next != NULL) {
      if (new < freeTmp->next) {
         break;
      }
      freeTmp = freeTmp->next;
   }

   new->next = freeTmp->next;
   freeTmp->next = new;
}


/*
 * CompactFreeList --
 *
 *      Defragment memory by merging all adjacent free blocks.
 */

void
CompactFreeList(void)
{
   struct MemHeader *freeTmp = freeList;

   /*
    * Search through the free list for adjacent blocks - if two are
    * adjacent, then merge them to create a larger block.
    */

   while (freeTmp != NULL) {
      if (freeTmp->next == NULL) {
         /* at the end of the list */
         break;
      }

      /* if two free blocks are adjacent */
      if ((void *)freeTmp + freeTmp->size + sizeof(struct MemHeader) ==
          (void *)freeTmp->next) {

         /* merge them */
         freeTmp->size += freeTmp->next->size + sizeof(struct MemHeader);
         freeTmp->next = freeTmp->next->next;

         continue; /* try again with this same block */
      }

      freeTmp = freeTmp->next;
   }

   freeListNeedsCompacting = FALSE;
}


/*
 * MemInit --
 *
 *      Called when Alloc is first invoked.  Sets up the necessary
 *      data structures.
 */

void
AllocInit(void)
{
   chunkSize = DEFAULT_CHUNK_SIZE;

   /* set up the system list */
   memory = (void *)RegionAlloc(MM_VA_HEAP, chunkSize / PAGE_SIZE, MM_RW);
   memory->next = NULL;

   /* set up the free list */
   freeList = (void *)memory + sizeof(struct SysBlock);
   freeList->size = (chunkSize - sizeof(struct SysBlock) -
                     sizeof(struct MemHeader));
   freeList->next = NULL;
}


/*
 * Alloc --
 *
 *      Allocate AMOUNT bytes of memory and return it with all of its
 *      bytes zeroed.
 */

void *
alloc(size_t amount)
{
   struct MemHeader *freeTmp, *last, *retBlock;
   struct SysBlock *sysTmp;

   if (amount == 0) {
      return NULL;
   }

   chunkSize = DEFAULT_CHUNK_SIZE;

   /* if more than the default chunk size was requested */
   if (amount > MAX_ALLOC_AMOUNT) {
      chunkSize = amount + ALLOC_ADJUST;
   }

   chunkSize = ROUND_TO_PAGE_SIZE(chunkSize);

   if (freeList == NULL && allocList == NULL) {
      AllocInit();
   }

   /* don't allocate less than MIN_ALLOC_AMOUNT */
   if (amount < MIN_ALLOC_AMOUNT) {
      amount = MIN_ALLOC_AMOUNT;
   }


   /* loop until a block is found */
   while (TRUE) {
      /* first walk the free list, looking for the correct size */
      last = NULL;
      freeTmp = freeList;

      while (freeTmp != NULL) {
         if (freeTmp->size >= amount) {
            goto GOT_FREE_BLOCK;
         }

         last = freeTmp;
         freeTmp = freeTmp->next;
      }


      /*
       * Didn't find a free block of the right size - try compacting
       * the free list.
       */
      if (freeListNeedsCompacting) {
         CompactFreeList();
         continue;
      }

      /* if we got here, then we need to allocate a new chunk */

      /* find the last chunk */
      sysTmp = memory;
      while (sysTmp->next != NULL) {
         sysTmp = sysTmp->next;
      }

      /* alloc a new chunk and save its ptr */
      sysTmp->next = (void *)RegionAlloc(MM_VA_HEAP,
                                         chunkSize / PAGE_SIZE, MM_RW);
      sysTmp->next->next = NULL;

      /* create the new free block */
      freeTmp = (void *)sysTmp->next + sizeof(struct SysBlock);
      freeTmp->next = NULL;
      freeTmp->size = chunkSize - ALLOC_ADJUST;

      /* put that bad boy in the free list */
      FreeListInsertSorted(freeTmp);

      /* with this new large chunk, try again */
   }

  GOT_FREE_BLOCK:
   /* remove the block from the free list */
   if (last == NULL) {
      freeList = freeTmp->next;
   } else {
      last->next = freeTmp->next;
   }

   retBlock = freeTmp;


   /* shrink the block, if possible */
   if (retBlock->size >
       amount + sizeof(struct MemHeader) + MIN_ALLOC_AMOUNT) {
      struct MemHeader *new = ((void *)retBlock + sizeof(struct MemHeader) +
                               amount);

      new->size = retBlock->size - sizeof (struct MemHeader) - amount;
      new->next = NULL;
      retBlock->size = amount;

      FreeListInsertSorted(new);
   }

   /* put it on the alloc list */
   retBlock->next = allocList;
   allocList = retBlock;


   /* and (finally) return the memory */
   bzero((void *)retBlock + sizeof(struct MemHeader), amount);
   return ((void *)retBlock + sizeof(struct MemHeader));
}

/*
 * Free --
 *
 * Add FREEMEM to the free list.
 *
 */

void
free(void *freemem)
{
   struct MemHeader *mem;
   struct MemHeader *tmp = allocList, *last = NULL;

   if (freemem == NULL) {
      return;
   }

   mem = freemem - sizeof(struct MemHeader);

   /* find FREEMEM on the alloc list */
   while (tmp != NULL) {
      if (tmp == mem) break;

      last = tmp;
      tmp = tmp->next;
   }

   /* remove from the alloc list */
   if (last == NULL) {
      allocList = tmp->next;
   } else {
      last->next = tmp->next;
   }

   /* put it on the free list */
   FreeListInsertSorted(mem);
   freeListNeedsCompacting = TRUE;
}


void *
realloc(void *mem, size_t amt)
{
   void *new;

   new = alloc(amt);

   if (new != NULL) {
      bcopy(mem, new, amt);
      free(mem);
   }

   return new;
}
