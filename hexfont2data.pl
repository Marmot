#!/usr/bin/perl
#
# hexfont2data.pl --
#
#      Convert a font hex file (http://unifoundry.com/unifont.html) to
#      a .data section suitable for assembly by gas.

use strict;
use warnings;

my $in = *STDIN;
my $out = *STDOUT;

if (@ARGV == 0) {
   # nop
} elsif (@ARGV == 2) {
   open $in, "<$ARGV[0]" or die "Unable to read $ARGV[0]: $!\n";
   open $out, ">$ARGV[1]" or die "Unable to write $ARGV[1]: $!\n";
} elsif (@ARGV == 1) {
   my $type;

   if (-e $ARGV[0] and (stat($ARGV[0]))[7]) {
      $type = 'input';
      open $in, "<$ARGV[0]" or die "Unable to read $ARGV[0]: $!\n";
   } else {
      $type = 'output';
      open $out, ">$ARGV[0]" or die "Unable to write $ARGV[0]: $!\n";
   }

   print STDERR "Warning: ambiguous arguments.  Assuming $ARGV[0] is $type\n";
} else {
   die <<"USAGE";
Usage: hexfont2data.pl [input] [output]
USAGE
}

my $table = '';
my $data = '';
my $c = 0;

while (my $line = <$in>) {
   chomp $line;
   $c++;

   unless ($line =~ m/^(....):(.+)/) {
      print STDERR "Warning: Invalid line $c\n";
      next;
   }

   my ($n, $bmp) = ($1, $2);
   my ($width, $height);

   my $length = length($bmp) / 2;

   # XXX: I count 32 and 64, but Perl counts 16 and 32(!).  I'm baffled.
   if ($length == 16) {
      $width = 8;
      $height = 16;
   } elsif ($length == 32) {
      $width = 16;
      $height = 16;
   }

   $table .= <<"TABLE_ENTRY";
        .long   G$n
        .word   $width, $height
TABLE_ENTRY

   my $bytes = "G$n:  .byte   ";
   while ($bmp =~ m/(..)/g) {
      $bytes .= "0x$1,";
   }
   chop $bytes; # remove trailing comma

   $data .= "$bytes\n";
}

print $out <<"DATA";
        .section .data
        .align  8
        .global fontTable
fontTable:
$table

        .align 4
$data
DATA
