        /* boot loader for Marmot */

	/* 2k stack starting at 0x6000 */
#define BOOT_SS      0x400
#define BOOT_SP      0x2000

#define LOADER_START 0x8000


	.section .boot, "xa"

	.code16
	.global _start
_start: jmp     around


	/* BIOS parameter block */

	.org    3
oem_signature:
        .ascii  "=marmot="

	/* DOSv2 */
sector_size:
        .word   512
sectors_per_allocation_unit:
        .byte   2
reserved_sectors:
        .word   2
fat_count:
        .byte   0
record_count:
        .word   2
sectors_in_volume:
        .word   2880
media_descriptor:
        .byte   0xf0    /* 3.5" floppy */
sectors_per_fat:
        .word   0

        /* DOSv3.4 */
sectors_per_track:
        .word   0
head_count:
        .word   0
hidden_sector_count:
        .long   0
sectors_in_volume_long:
        .long   0

	/* Marmot format */

	.global loaderStart
loaderStart:
        .word   0
        .global loaderSectors
loaderSectors:
        .word   0
        .global bootPreferredVideoMode
bootPreferredVideoMode:
        .word   0x0144          /* 1280x1024x32bpp */


around: cli
        ljmp    $0, $boing
boing:  xor     %ax, %ax
        mov     %ax, %ds
        mov     %ax, %es
	lss     BOOT_SS_SP, %sp
	sti

        mov     %dl, bootDevice
        call    VideoInit
        call    LoadRest
	jmp     Loader


        /*
         * VideoInit --
         *
         *      Initialize the display.
         *
         */
VideoInit:
	/* 80x25 16 colors */
        movb    $0x03, %al      /* ah is already zeroed */
        int     $0x10
        /* disable cursor */
        mov     $1, %ah
        mov     $0x2000, %cx
        int     $0x10

        xor     %dx, %dx
        call    MoveCursor
        ret


	/*
         * MoveCursor --
         *
         *      Position the cursor to the coordinates in dx.
         *
         */

MoveCursor:
        mov     $2, %ah
        mov     $1, %bh
        int     $0x10
	ret


        /*
         * PrintMessage --
         *
         *      Print the message pointed to by si.
         *
         */

PrintMessage:
        mov     $0x0e, %ah
        mov     $0x0100, %bx
1:      lodsb
        test    %al, %al
        jz      1f
        int     $0x10
        jmp     1b
1:      ret


        /*
         * LoadRest --
         *
         *      Load the rest of the boot loader from disk into memory.
         *
         */

LoadRest:
	call    ResetDevice

	/* Get driver parameters */
        mov     $8, %ah
	mov     bootDevice, %dl
        int     $0x13
        jnc     1f

        mov     $noParams, %si
        jmp     Die

1:      /* Important values from int 13,8 are:
         *    cx[5:0]  - sectors per track
	 *    dh       - heads
	 */

	movb    %dh, driveHeads
	and     $0x3f, %cx
        movb    %cl, sectorsPerTrack

        mov     esSave, %es
	xor     %bx, %bx
	xor     %ax, %ax

ReadLoop:
        inc     %ax
        cmpw    loaderSectors, %ax
        jbe     1f

        /* All done */
	xor     %ax, %ax
        mov     %ax, %es
	ret

1:      mov     $3, %dx
        mov     %dx, tries
        mov     %ax, sector

_retry:
        call    LBA2CHS

	mov     $0x0201, %ax
	mov     bootDevice, %dl
        int     $0x13
        jnc     1f
	
        /* read failed - reset and try again */
	mov     sector, %ax
	decw    tries
        jnz     _retry

	/* oops - too many tries */
	mov     $readFailed, %si
        jmp     Die

1:      addw    $0x20, esSave
        mov     esSave, %es
        mov     sector, %ax
        jmp     ReadLoop


	/*
         * LBA2CHS --
         *
         *      Convert the block number in ax to a CHS value in cx and dh.
         *
         *      Clobbers ax, cx, dx
         */
	
	.global LBA2CHS
LBA2CHS:
        xor     %dx, %dx
        divw    sectorsPerTrack
	mov     %dx, chsScratch

        xor     %dx, %dx
        divw    driveHeads

        /* Values are:
         *
	 *     (%sp) sector
         *     ax    cylinder
         *     dx    head
         */

        shl     $8, %dx         /* dh => head */
        mov     %ax, %cx
        shl     $8, %cx         /* ch => low 8 bits of cylinder */
        shr     $2, %ax
        and     $0xc0, %al
	movb    chsScratch, %cl
	inc     %cl
        and     $0x3f, %cl      /* cl[5:0] => sector */
        or      %al, %cl        /* cl[7:6] => high 2 bits of cylinder */

	.global LBA2CHS_Done
LBA2CHS_Done:
        ret


        /*
         * ResetDevice --
         *
         *      Reset the disk controller for the boot device.
         *
         */

ResetDevice:
	mov     bootDevice, %dl
	mov     $0, %ah
        int     $0x13
        jc      1f
	ret

1:      mov     $resetFailed, %si

        /* fall through */


	/*
         * Die --
         *
         *      Print the message pointed to by %si and halt.
         */

	.global Die             /* so it's visible to VProbes */
Die:    xor     %dx, %dx
        call    MoveCursor
        push    %si
        mov     $diskn, %si
        call    PrintMessage
	pop     %si
        call    PrintMessage
        mov     $failed, %si
	pushw   $halt
        jmp     PrintMessage

halt:   hlt
        jmp     halt


	/* %ss/%sp values for lss - saves a byte over two movs */
BOOT_SS_SP:
        .word   BOOT_SP, BOOT_SS
        /* %es value during load loop */
esSave:
        .word   LOADER_START >> 4
        /* start of string for Die */
diskn:
        .asciz  "Disk "
        /* end of string for Die */
failed:
        .asciz  " failed!!!"
	/* various strings for error messages */
resetFailed:
        .asciz  "reset"
readFailed:
        .asciz  "read"
noParams:
        .asciz  "get params"


	/* Master boot record */

	.org    0x1b8
disk_signature:
	.long   0xbebaefbe
        .word   0
partitions:
        .fill   64, 1, 0
signature:
        .byte   0x55, 0xaa


	/*
         * .boot_bss --
         *
         *      Scratch area for the boot loader located at 0x7e00.
         *
         */

        .section .boot_bss
bootDevice:
        .byte   0
driveHeads:
        .byte   0
sectorsPerTrack:
        .byte   0
tries:
        .byte   0
sector:
        .word   0
chsScratch:
        .word   0
