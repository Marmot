/*
 * task.c --
 *
 *      Basic task handling.
 */


#include <marmot.h>

typedef enum {
   SCHED_PRIORITY_IRQ,
   SCHED_PRIORITY_INTERRUPT,
   SCHED_PRIORITY0,
   SCHED_PRIORITY1,
   SCHED_PRIORITY2,
   SCHED_PRIORITY3,
   SCHED_PRIORITY4,
   SCHED_PRIORITY5,
   SCHED_PRIORITY6,
   SCHED_PRIORITY7
} SchedPriority;


typedef struct {
   UReg rax;
   UReg rbx;
   UReg rcx;
   UReg rdx;
   UReg rdi;
   UReg rsi;
   UReg rbp;
   UReg rsp;
   UReg r8;
   UReg r9;
   UReg r10;
   UReg r11;
   UReg r12;
   UReg r13;
   UReg r14;
   UReg r15;
} Registers;


struct task {
   uint64 id;
   Registers regs;
   UReg cr3;
};


void
SaveTask(void)
{

}

void
SwitchToTaskStack(void)
{

}

void
SwitchToTask(void)
{

}


typedef struct _DFCArgs {
   uint64 a1;
   uint64 a2;
   uint64 a3;
   uint64 a4;
} *DFCArgs;

typedef void (*DFCFcn)(DFCArgs);

typedef struct _DFC {
   struct _DFC *next;
   DFCFcn fcn;
   DFCArgs args;
} DFC;

DFC *dfcHead;


void
RunDFC(DFCFcn fcn, DFCArgs args)
{

}


/*
 * This is only called from the timer interrupt.  As it doesn't
 * return, it's necessary to unmask the timer IRQ prior to switching
 * to the new task.
 */

void
TaskSchedule(uint64 timerIrq)
{
   DFC *dfc;

   SaveTask();
   SwitchToTaskStack();

   /*
    * Run all DFCs now.
    */

   sti(); // timer irq is still masked

   for (dfc = dfcHead; dfc != NULL; dfc = dfcHead) {
      dfcHead = dfc->next;

      RunDFC(dfc->fcn, dfc->args);
      free(dfc->args);
      free(dfc);
   }

   /*
    * Run regular tasks now.
    */

   // XXX


   UnmaskPIC(timerIrq - IRQBASE);
   SwitchToTask(); // executes an iretq
}


typedef void (*Task)(void);

void
TaskStart(Task t)
{
   /*
    * Allocate new stack.
    */

}


void
TaskInit(void)
{
   dfcHead = NULL;

   /*
    * Create task stack
    * Create runqueues
    * Hook TaskSchedule into the timer interrupt.
    */
}




/*
 * Defer --
 *
 *      Create a deferred function call.  This DFC is placed on the
 *      runqueue of the specified priority and, once scheduled, is
 *      passed the appropriate args.
 *
 *      DFCs run synchronously on the stack of the scheduler which
 *      makes them especially convenient for IRQ/fault handling.
 *
 *      The DFCArgs that is passed 
 */

void
Defer(SchedPriority p, DFCFcn dfc, DFCArgs *args)
{
   
}
