#!/usr/bin/perl
#
# Parse link map and generate symbols.txt to use with VProbes.

use strict;
use warnings;

my $map = 'link.map';
die "Missing map file\n" unless -f $map;
open MAP, "<$map" or die "Unable to open $map: $!";

my %syms = ();

while (my $line = <MAP>) {
   if ($line =~ m/^\s+0x([0-9a-f]+)\s+(\w+)\s*$/) {
      $syms{$1} = $2;
#      print "$1 T $2\n";
   }
}

close MAP;

open SYMS, ">symbols.txt" or die "Unable to open symbols.txt: $!";

foreach my $addr (sort(keys(%syms))) {
   my $sym = $syms{$addr};
   print SYMS "$addr T $sym\n";
}

close SYMS;
