/*
 * marmot.h --
 *
 *      Marmot definitions.
 */

#ifndef _MARMOT_H
#define _MARMOT_H

#include <types.h>
#ifndef ASM
#include <cpu.h>
#endif

/*
 * Used for the scheduler.  Timekeeping is controlled by the RTC
 * (until high precision timing is needed).
 */

#define TICK_Hz 64


/*
 * e820 memory map space is statically allocated in .bss.  64 entries
 * *should* be enough.
 */

#define MAX_E820 64

/* "SMAP" */
#define SMAP     0x534d4150

/* "_MP_" */
#define MP_MAGIC 0x5f504d5f


/*
 * Memmap for init code:
 *
 *       0 - 0x0fff  BIOS data area
 *   0x1000 - 0x1f00  IDT (240 entries)
 *   0x1f00 - 0x2000  GDT (16 entries)
 *   0x2000 - 0x3000  Page map level 4
 *   0x3000 - 0x4000  Page directory pointer table
 *   0x4000 - 0x5000  Page directory
 *   0x5000 - 0x6000  Page table
 *   0x5800 - 0x6000  Stack (real mode)
 *   0x6000 - 0x7f00  bss
 *   0x7f00 - 0x8000  TSS (0x68 + 8 bytes == rounded up 0x100)
 *   0x7000 - 0x8000  Boot code
 *   0x8000 - 0x10000 Loader
 *  0x10000 - 0x18000 Stack (protected/long mode)
 *
 */

#define IDT      0x1000
#define GDT      0x1f00
#define TSS      0x7f00

#define PML4BASE 0x2000
#define PDPTBASE 0x3000
#define PDBASE   0x4000
#define PTBASE   0x5000
#define PAGETOP  0x6000

#define STACKTOP 0x18000

/*
 * Descriptors.
 */

#define CS32     0x0008
#define CS64     0x0010
#define DS       0x0018
#define TS       0x0020
#define CSPRIV   0x0032
#define DSPRIV   0x003a
#define CSUSER   0x0043
#define DSUSER   0x004b


/*
 * System virtual memory layout.
 */

/* Loader uses identity mapped pages. */
#define MM_VA_LOADER_START 0

/* Kernel gets from 4GB to PRIV_START */
#define MM_VA_KERNEL_START 0x0000000100000000

/* Privileged processes get from 64GB to USER_START */
#define MM_VA_PRIV_START   0x0000001000000000

/* User gets the rest. */
#define MM_VA_USER_START   0x0000100000000000

/* Top of canonical address space */
#define MM_VA_CANONICAL_TOP 0x0000800000000000


/*
 * Magic values for PageAlloc and GetFreePA.
 */

#define MM_VA_DONT_CARE (-1ULL)
#define MM_VA_INVALID   (-2ULL)
#define MM_VA_IDENT     (-3ULL)
#define MM_PA_INVALID   (-4ULL)
#define MM_VA_HEAP      (-5ULL)

#define MM_RW (1 << 1)
#define MM_NX (1 << 63)

/*
 * Physmem pools used by GetFreePA.
 */

#define POOL_IDENT      0
#define POOL_KERNEL     1
#define POOL_PRIVILEGED 2
#define POOL_USER       3


#define NULL 0

#define PAGE_SIZE 4096


/*
 * Various kernel-level stacks.  All are allocated just below the top
 * of kernel memory.
 *
 *      FAULT_STACK     - 2 page stack used by normal fault handlers
 *      CRITICAL_STACK  - 4 page stack for #MC, #DF and #NMI.
 *      IRQ_STACK       - 2 page stack for all IRQs
 *      KERNEL_STACK    - Regular kernel stack.  Grows as needed.
 */

#define FAULT_STACK_TOP       MM_VA_PRIV_START
#define FAULT_STACK_BOTTOM    (FAULT_STACK_TOP - 2 * PAGE_SIZE)

#define CRITICAL_STACK_TOP    FAULT_STACK_BOTTOM
#define CRITICAL_STACK_BOTTOM (CRITICAL_STACK_TOP - 4 * PAGE_SIZE)

#define IRQ_STACK_TOP         CRITICAL_STACK_BOTTOM
#define IRQ_STACK_BOTTOM      (IRQ_STACK_TOP - 2 * PAGE_SIZE)

#define KERNEL_STACK_TOP      IRQ_STACK_BOTTOM


/* #define KERNEL_TEXT_START     0x0000000100000000 */
/* #define KERNEL_DATA_START     0x0000000100000000 */
/* #define KERNEL_BSS_START      0x0000000100000000 */
#define KERNEL_HEAP_START     0x0000000800000000
#define MM_VA_KERNEL_HEAP     KERNEL_HEAP_START
#define PRIV_HEAP_START       0x0000008000000000
#define MM_VA_PRIV_HEAP       PRIV_HEAP_START
#define USER_HEAP_START       0x0000800000000000
#define MM_VA_USER_HEAP       USER_HEAP_START


/* MSRs */
	
/* aka IA32_TIME_STAMP_COUNTER */
#define TSC_MSR       0x10

/* aka IA32_APIC_BASE */
#define APIC_BASE_MSR 0x1b

/* aka IA32_EFER */
#define EFER          0xc0000080

/* syscall enable (r/w) */
#define EFER_SCE      0

/* long mode enable (r/w) */
#define EFER_LME      8

/* long mode active (r) */
#define EFER_LMA      10

/* execute disable bit enable (r/w) */
#define EFER_NXE      11


/*
 * IRQ vectors.  These are the same in both PIC/APIC mode.
 */

#define IRQBASE     0x20
#define PIT_IRQ     0x20
#define KBD_IRQ     0x21
#define SERIAL1_IRQ 0x23
#define SERIAL0_IRQ 0x24
#define FDD_IRQ     0x26
#define PIO_IRQ     0x27
#define RTC_IRQ     0x28
#define MOUSE_IRQ   0x2c


#define MOUSE_SAMPLE_RATE 100


/*
 * RGBA color values.
 */

#define COLOR_INVALID         0x00ffffff

#define COLOR_BLACK           0xff000000
#define COLOR_WHITE           0xffffffff
#define COLOR_RED             0xffff0000
#define COLOR_GREEN           0xff00ff00
#define COLOR_BLUE            0xff0000ff
#define COLOR_PLEASING_GREEN  0xff73dba2
#define COLOR_PURPLE          0xffff00ff
#define COLOR_ROBINs_PURPLE   0xff8800ff

#define BOOT_BACKGROUND_COLOR COLOR_PLEASING_GREEN


#ifndef ASM

typedef struct {
   uint64 errorCode;
   uint64 rip;
   uint64 cs;
   uint64 rflags;
   uint64 rsp;
   uint64 ss;
} __attribute__((packed)) ExcFrame;


/* global vprobe point for GUEST:test */
extern volatile uint64 tval, tval2;
#define TEST_VPROBE(v1,v2) do { tval = (v1); tval2 = (v2); \
                                asm("sfence\n.global test\ntest:\n");  \
                              } while (0)


#define BUG_ASSERT        0
#define BUG_UNIMPLEMENTED 1
#define BUG_MISC          2

#define ASSERT(c) if (!(c)) { Bug(BUG_ASSERT, #c, __FILE__, __LINE__); }
#define UNIMPLEMENTED(what) Bug(BUG_UNIMPLEMENTED, what, __FILE__, __LINE__)
void Bug(uint64, char *, char *, uint64);

static inline void
cli(void)
{
   __asm__ __volatile__("cli");
}

static inline void
sti(void)
{
   __asm__ __volatile__("sti");
}

static inline void
outb(uint8 val, uint16 port)
{
   __asm__ __volatile__("outb %b0, %w1" : : "a" (val), "Nd" (port));
}

static inline uint8
inb(uint16 port)
{
   uint8 val;

   __asm__ __volatile__("inb %w1, %0" : "=a" (val) : "Nd" (port));
   return val;
}

static inline void
outl(uint32 val, uint16 port)
{
   __asm__ __volatile__("outl %0, %w1" : : "a" (val), "Nd" (port));
}

static inline uint32
inl(uint16 port)
{
   uint32 val;

   __asm__ __volatile__("inl %w1, %0" : "=a" (val) : "Nd" (port));
   return val;
}



#define invlpg(m) __asm__ __volatile__("invlpg (%0)":: "r" (m) : "memory");
#define rdmsr(msr,val) do { uint32 _a, _d; \
                            __asm__ __volatile__("rdmsr" \
                                                 : "=a" (_a), "=d" (_d) \
                                                 : "c" (msr));  \
                            val = (uint64)_a | ((uint64)_d << 32);\
                          } while (0)
#define wrmsr(msr,val) do { uint32 _a, _d; \
                            _a = (uint32)val; \
                            _d = (uint32)(val >> 32); \
                            __asm__ __volatile__("wrmsr" \
                                                 : \
                                                 : "c" (msr), "a" (_a), "d" (_d)); \
                          } while(0)


#define __init __attribute__((section(".loader")))

extern uint16 xResolution;
extern uint16 yResolution;

typedef uint64 CbID;
typedef void (*MouseCBFun)(uint8 *, uint64);

struct MouseCB {
   struct MouseCB *next;
   CbID id;
   MouseCBFun cb;
};

CbID InstallMouseCB(MouseCBFun cb);
void RemoveMouseCB(CbID id);


void bzero(void *s, uint64 n);
void bcopy(void *s, void *d, uint64 n);
void bfill(void *s, uint64 n, uint8 val);

uint64 strlen(char *s);

void *alloc(size_t amt);
void free(void *mem);

VA RegionAlloc(VA desired, uint64 nPages, uint64 flags);
VA PageAlloc(VA desired, uint64 flags);
void MapIORegion(PA start, PA end, char *name);


void TaskSchedule(uint64);


void SetPixel(Color color, Point p);
void PrintMessage(Color color, Point where, char *msg);
void ColorRectangle(Color color, Point c0, Point c1);
void ColorTriangle(Color color, Point c0, Point c1, Point c2);
void ColorCircle(Color color, Point center, Length radius);


void MaskPIC(uint8 irq);
void UnmaskPIC(uint8 irq);


/*
 * Stub routines in interrupt.S
 */

void _IRQStub_PIT(void);
void _IRQStub_KBD(void);
void _IRQStub_RTC(void);
void _IRQStub_MOUSE(void);

#endif /* !ASM */
#endif
