/*
 * video.c --
 *
 *      Video interface.
 */

#include <marmot.h>


struct FontTable {
   /* glyph is the 32 bit address of the glyph */
   uint32 glyph;
   /* width and height of the glyph in pixels */
   uint16 w;
   uint16 h;
} __attribute__((packed));

extern struct FontTable fontTable[];

extern uint8 *frameBuffer;
extern uint64 frameBufferSize;

extern uint32 physBasePtr;
extern uint8 bitsPerPixel;
extern uint16 xResolution;
extern uint16 yResolution;
extern uint8 redMaskSize;
extern uint8 redFieldPosition;
extern uint8 greenMaskSize;
extern uint8 greenFieldPosition;
extern uint8 blueMaskSize;
extern uint8 blueFieldPosition;
extern uint8 rsvdMaskSize;
extern uint8 rsvdFieldPosition;

static uint64 rMask;
static uint64 gMask;
static uint64 bMask;
static uint64 aMask;


/*
 * SetPixel --
 *
 *      Set the pixel at x,y to the specified ARGB color.
 */

void
SetPixel(Color color, Point p)
{
   uint32 pixelValue = 0;
   int bytesPerPixel = 1;
   uint8 *pixel;


   /* Convert color into local colorspace. */

   switch (bitsPerPixel) {
   case 32:
   case 24:
      bytesPerPixel = 4;
      pixelValue = (uint32)color;
      break;
   case 16:
   case 15:
      /* 5:6:5 */
      /* 1:5:5:5 */
      pixelValue = (uint32)(((color >> redFieldPosition) & rMask) |
                            ((color >> greenFieldPosition) & gMask) |
                            ((color >> blueFieldPosition) & bMask) |
                            ((color >> rsvdFieldPosition) & aMask));
      bytesPerPixel = 2;
      break;
   case 8:
   case 4:
      UNIMPLEMENTED("8 or 4 bits per pixel");
      pixelValue = 0x80;
      bytesPerPixel = 1;
   }

   /* Find pixel address */
   pixel = frameBuffer + bytesPerPixel * (p.x + p.y * xResolution);

   switch (bytesPerPixel) {
   case 4:
      *((uint32 *)pixel) = pixelValue;
      break;
   case 2:
      *((uint16 *)pixel) = (uint16)pixelValue;
      break;
   case 1:
      *((uint8 *)pixel) = (uint8)pixelValue;
      break;
   }
}


Color
GetPixel(Point p)
{
   Color c = COLOR_BLACK;
   uint16 pix16;
   uint8 *pixel;

   switch (bitsPerPixel) {
   case 32:
   case 24:
      pixel = frameBuffer + 4 * (p.x + p.y * xResolution);
      c = *((uint32 *)pixel);
      break;
   case 16:
   case 15:
      pixel = frameBuffer + 2 * (p.x + p.y * xResolution);
      pix16 = *((uint16 *)pixel);
      c = (((pix16 & rMask) << redFieldPosition) |
           ((pix16 & gMask) << greenFieldPosition) |
           ((pix16 & bMask) << blueFieldPosition) |
           ((pix16 & aMask) << rsvdFieldPosition));
      break;
   case 8:
   case 4:
      UNIMPLEMENTED("8 or 4 bits per pixel");
      break;
   }

   return c;
}


/*
 * ColorRectangle --
 *
 *      Set a rectangular area to be the specified color.
 */

void
ColorRectangle(Color color, Point c0, Point c1)
{
   uint64 xLow, xHigh, yLow, yHigh, x, y;

   if (c0.x == c1.x || c0.y == c1.y) {
      return;
   }

   if (c0.x < c1.x) {
      xLow = c0.x;
      xHigh = c1.x;
   } else {
      xLow = c1.x;
      xHigh = c0.x;
   }
   if (c0.y < c1.y) {
      yLow = c0.y;
      yHigh = c1.y;
   } else {
      yLow = c1.y;
      yHigh = c0.y;
   }

   for (x = xLow; x <= xHigh; x++) {
      for (y = yLow; y <= yHigh; y++) {
         Point p;

         p.x = x;
         p.y = y;

         SetPixel(color, p);
      }
   }
}

void
ColorCircle(Color color, Point c, Length r)
{
   uint64 xl, xr, yt, yb;
   Point p;

   if (r == 0) {
      return;
   }

   /* correct borders */
   if (r > c.x) {
      xl = r - c.x;
   } else {
      xl = 0;
   }
   if (c.x + r > xResolution - 1) {
      xr = xResolution - 1;
   } else {
      xr = c.x + r;
   }
   if (r > c.y) {
      yt = r - c.y;
   } else {
      yt = 0;
   }
   if (c.y + r > yResolution - 1) {
      yb = yResolution - 1;
   } else {
      yb = c.y + r;
   }

   for (p.x = xl; p.x <=xr; p.x++) {
      for (p.y = yt; p.y <= yb; p.y++) {
         uint64 xx, yy;

         /* limit x and y to quadrant 4 */
         if (p.x < c.x) {
            xx = c.x - p.x;
         } else {
            xx = p.x - c.x;
         }
         if (p.y < c.y) {
            yy = c.y - p.y;
         } else {
            yy = p.y - c.y;
         }

         if (xx * xx + yy * yy < r * r) {
            SetPixel(color, p);
         }
      }
   }
}


/*
 * SetBootBackground --
 *
 *      Set the screen background to the default boot color.
 */

void
SetBootBackground(void)
{
   Point p;

   for (p.x = 0; p.x < xResolution; p.x++) {
      for (p.y = 0; p.y < yResolution; p.y++) {
         SetPixel(BOOT_BACKGROUND_COLOR, p);
      }
   }
}


/*
 * VideoInit --
 *
 *      Calculate frame buffer size and map it into the virtual
 *      address space.
 */

void
VideoInit(void)
{
   uint64 bytesPerPixel = 0;

   /*
    * Identity map the frame buffer.
    */

   frameBuffer = (uint8 *)(VA)physBasePtr;

   switch (bitsPerPixel) {
   case 32:
   case 24:
      bytesPerPixel = 4;
      break;
   case 16:
   case 15:
      bytesPerPixel = 2;
      break;
   default:
      bytesPerPixel = 1;
   }

   frameBufferSize = xResolution * yResolution * bytesPerPixel;

   MapIORegion((PA)frameBuffer,
               (PA)(frameBuffer + frameBufferSize),
               "frameBuffer");


   /*
    * Init pixel masks.
    */

   rMask = ((1ULL << redMaskSize) - 1) << redFieldPosition;
   gMask = ((1ULL << greenMaskSize) - 1) << greenFieldPosition;
   bMask = ((1ULL << blueMaskSize) - 1) << blueFieldPosition;
   aMask = (rsvdMaskSize ? (1ULL << rsvdMaskSize) - 1 : 0) << rsvdFieldPosition;


   /*
    * Set a pleasing background color.
    */

   SetBootBackground();
}


#define PIXEL_VALUE(glyph,p) ((((glyph)[(p) >> 3]) &            \
                               (1 << (7 - ((p) % 8)) )) >>      \
                              (7 - ((p) % 8)))

/*
 * PrintMessage --
 *
 *      Display msg at pixel coordinates x,y.
 */

void
PrintMessage(Color color, Point where, char *msg)
{
   int c;
   uint64 xOff = 0; /* offset from start of message */

   for (c = 0; msg[c]; c++) {
      uint8 *glyph;
      uint64 xp, yp;
      int p, ch;

      ch = msg[c];

      glyph = (uint8 *)((VA)fontTable[ch].glyph);
      xp = yp = 0;

      /* loop over pixels */
      for (p = 0; p < fontTable[ch].w * fontTable[ch].h; p++) {

         if (PIXEL_VALUE(glyph, p) == 1) {
            Point p;

            p.x = where.x + xOff + xp;
            p.y = where.y + yp;

            SetPixel(color, p);
         }

         xp++;
         if ((xp % fontTable[ch].w) == 0) {
            xp = 0;
            yp++;
         }
      }

      xOff += fontTable[ch].w;
   }
}

/*
 * Cursor handling.
 *
 * Register a callback with irq12 (will be a deferred function call).
 * When the cursor moves, the old cursor spot is replaced with its
 * saved spot, then the new cursor spot is saved and the cursor is
 * drawn.
 *
 */

#include "cursor.xbm"

static Color savedCursor[cursor_width][cursor_height];
static struct {
   int32 x;
   int32 y;
   int32 z;
   unsigned int button1:1;
   unsigned int button2:1;
   unsigned int button3:1;
   unsigned int button4:1;
   unsigned int button5:1;
} cursorInfo;


static CbID cursorCbId;

void
RestoreCursorArea(void)
{
   int32 x, y, sx, sy;
   Point p;

   for (x = 0; x < cursor_width; x++) {
      for (y = 0; y < cursor_height; y++) {
         sx = cursorInfo.x - cursor_x_hot + x;
         sy = cursorInfo.y - cursor_y_hot + y;

         if (sx < 0 || sy < 0 || sx > xResolution || sy > yResolution) {
            continue;
         }

         p.x = sx;
         p.y = sy;
         SetPixel(savedCursor[x][y], p);
      }
   }
}


void
SaveCursorArea(void)
{
   int32 x, y, sx, sy;
   Point p;

   for (x = 0; x < cursor_width; x++) {
      for (y = 0; y < cursor_height; y++) {
         sx = cursorInfo.x - cursor_x_hot + x;
         sy = cursorInfo.y - cursor_y_hot + y;

         if (sx < 0 || sy < 0 || sx > xResolution || sy > yResolution) {
            continue;
         }

         p.x = sx;
         p.y = sy;
         savedCursor[x][y] = GetPixel(p);
      }
   }
}


/* assume 16x16 */
#define CURSOR_ISSET(x,y) ((cursor_bits[(x)] >> y) & 1)

void
DisplayCursor(void)
{
   int32 x, y, sx, sy;
   Point p;

   for (x = 0; x < cursor_width; x++) {
      for (y = 0; y < cursor_height; y++) {
         sx = cursorInfo.x - cursor_x_hot + x;
         sy = cursorInfo.y - cursor_y_hot + y;

         if (sx < 0 || sy < 0 || sx > xResolution || sy > yResolution) {
            continue;
         }

         p.x = sx;
         p.y = sy;

         if (!CURSOR_ISSET(x,y)) {
            continue;
         }

         SetPixel(COLOR_BLACK, p);
      }
   }
}


void
DoButtons(int b1, int b2, int b3)
{
   Point p;
   Length r;

   p.x = 520;
   p.y = 25;
   r = 20;

   if (b1) {
      ColorCircle(COLOR_BLUE, p, r);
   } else {
      ColorCircle(COLOR_PLEASING_GREEN, p, r);
   }
   p.x += 40;
   if (b2) {
      ColorCircle(COLOR_ROBINs_PURPLE, p, r);
   } else {
      ColorCircle(COLOR_PLEASING_GREEN, p, r);
   }
   p.x += 40;
   if (b3) {
      ColorCircle(COLOR_RED, p, r);
   } else {
      ColorCircle(COLOR_PLEASING_GREEN, p, r);
   }
}


void
DoCursor(uint8 *packet, uint64 N)
{
   int32 xd = 0, yd = 0;

   RestoreCursorArea();

   /* update coordinates */
   /*
    * N == 3
    *
    *     Byte 1: Yof Xof Ys Xs 1 b1 b2 b0
    *     Byte 2: Xmove
    *     Byte 3: Ymove
    *
    * N == 4
    *
    *     Byte 1: Yof Xof Ys Xs 1 b1 b2 b0
    *     Byte 2: Xmove
    *     Byte 3: Ymove
    *     Byte 4: Zmove
    */

   xd |= packet[1];
   if (packet[0] & 0x10) {
      xd |= 0xffffff00;
   }
   yd |= packet[2];
   if (packet[0] & 0x20) {
      yd |= 0xffffff00;
   }

   /* XXX do Z */

   cursorInfo.x += xd;
   cursorInfo.y -= yd;

   if (cursorInfo.x < 0) {
      cursorInfo.x = 0;
   } else if (cursorInfo.x > xResolution) {
      cursorInfo.x = xResolution;
   }
   if (cursorInfo.y < 0) {
      cursorInfo.y = 0;
   } else if (cursorInfo.y > yResolution) {
      cursorInfo.y = yResolution;
   }

   cursorInfo.button1 = packet[0] & 1;
   cursorInfo.button2 = (packet[0] >> 2) & 1;
   cursorInfo.button3 = (packet[0] >> 1) & 1;

   /* draw some small circles */
   DoButtons(cursorInfo.button1, cursorInfo.button2, cursorInfo.button3);

   SaveCursorArea();
   DisplayCursor();
}


void
CursorInit(void)
{
   cursorInfo.x = xResolution / 2;
   cursorInfo.y = yResolution / 2;
   cursorInfo.z = 0;
   cursorInfo.button1 = 0;
   cursorInfo.button2 = 0;
   cursorInfo.button3 = 0;
   cursorInfo.button4 = 0;
   cursorInfo.button5 = 0;

   SaveCursorArea();
   DisplayCursor();

   cursorCbId = InstallMouseCB(DoCursor);
}
