#!/usr/bin/perl
#
# cs.pl --
#
#      Calculate sectors and save to loaderSectors

use strict;
use warnings;

my $off = 0;

my $map = "link.map";
open MAP, "<$map" or die "Unable to open $map: $!";
while (my $line = <MAP>) {
   if ($line =~ m/(0x[0-9a-f]+)\s+loaderSectors/) {
      $off = hex($1) - 0x7c00;
      last;
   }
}

my $bytes = (stat('boot'))[7];
my $sectors = int(($bytes + 512) / 512) - 1;

my $image = "boot.img";

open IMG, "+<$image" or die "Unable to open $image: $!";
binmode IMG;
seek IMG, $off, 0;

my $n = syswrite IMG, pack('S', $sectors), 2;

close IMG;

print sprintf("  ==> Writing 0x%04x to loaderSectors at offset 0x%x\n",
              $sectors, $off);
