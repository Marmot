# Makefile for Marmot

CC = /usr/bin/gcc
LD = /usr/bin/ld
PERL = /usr/bin/perl

ASFLAGS = -Wa,--fatal-warnings
CFLAGS = -Wall -O2 -ffreestanding -funsigned-char -fno-common -I. -I.
LDFLAGS = -Map link.map --script=ld.conf

SFILES = boot.S loader.S interrupts.S vesa.S font.S util.S
CFILES = mm.c video.c irq.c task.c apic.c cpu.c pci.c alloc.c panic.c


.PHONY : all
all : marmot.iso

# Run this rule manually - should never need to be done
.PHONY : fonts
fonts :
	$(PERL) hexfont2data.pl basic_font.hex font.S

marmot.iso : boot.img
	cp -f boot.img iso_files/
	genisoimage -b boot.img -o marmot.iso iso_files

boot.img : boot
	$(PERL) mksym.pl
	dd if=/dev/urandom of=$@ bs=512 count=2880
	dd if=boot of=$@ bs=512 seek=0 conv=notrunc
	$(PERL) cs.pl

boot : $(SFILES:.S=.o) $(CFILES:.c=.o)
	$(LD) $^ $(LDFLAGS) -o $@

.PHONY : asm
asm : $(CFILES:.c=.s)

%.o : %.S
	$(CC) $(CFLAGS) $(ASFLAGS) -c -o $@ $<

%.s : %.c
	$(CC) $(CFLAGS) -S $<

.PHONY : clean
clean :
	rm -f boot boot.img marmot.iso link.map symbols.txt *.s *.o *~
