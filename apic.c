/*
 * apic.c
 *
 *      Check for APIC support and initialize external interrupts to use it.
 */

#include <marmot.h>

Bool usingAPIC;

void
InitPIC(void)
{
   usingAPIC = FALSE;
}

VA apicBase, ioapicBase;

/* uint64 tval, tval2; */

void
InitAPIC(void)
{
   uint64 apicBaseReg;
   char deviceName[] = {'l', 'a', 'p', 'i', 'c', 0, 0, 0, 0};

   if (!CPU_GetFeature(CPU_APIC_ENABLED)) {
      InitPIC();
      return;
   }

   usingAPIC = TRUE;

   rdmsr(APIC_BASE_MSR, apicBaseReg);
/*    VPROBE_TEST(APIC_BASE_MSR, apicBaseReg); */

   apicBase = (VA)(apicBaseReg & 0x000ffffffffff000);
   if (apicBase & (1 << 8)) {
      deviceName[5] = 'B';
      deviceName[6] = 'S';
      deviceName[7] = 'P';
   } else {
      deviceName[5] = 'A';
      deviceName[6] = 'P';
   }

   MapIORegion((PA)apicBase, (PA)apicBase + PAGE_SIZE, deviceName);

   /* init IO APIC (part of chipset) */
   ioapicBase = (VA)0xfec00000; // real address is in APICBASE on chipset

   
}
