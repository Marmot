/*
 * pci.c --
 *
 *      PCI device mapping and enumeration.
 */

#include <marmot.h>

/*
 * Format of a PCI address (pg 32):
 *
 *     Bit 31:     Enable bit
 *     Bits 24-30: Reserved MBZ
 *     Bits 16-23: Bus number (8?)
 *     Bits 11-15: Device number (32)
 *     Bits 8-10:  Function number (8)
 *     Bits 2-7:   Register number (64)
 *     Bit 1:      MBZ
 *     Bit 0:      MBZ
 *
 * PCI configuration space looks like (PCI LB2.3 pg195):
 *
 *     31                       16 15                               0
 *     |         Device ID        |             Vendor ID           |:0x00
 *     |           Status         |              Command            |:0x04
 *     | Base class |   Subclass  |   Registers   |   Revision  ID  |:0x08
 *     |   BIST     | Header Type | Latency Timer | Cache Line Size |:0x0c
 *     |                                                            |:0x10
 *     |                                                            |:0x14
 *     |                 Base Address Registers                     |:0x18
 *     |                                                            |:0x1c
 *     |                                                            |:0x20
 *     |                                                            |:0x24
 *     |                  Cardbus CIS Pointer                       |:0x28
 *     |   Subsystem Device ID    |        Subsystem Vendor ID      |:0x2c
 *     |                Expansion ROM base address                  |:0x30
 *     |                   Reserved          | Capabilities Pointer |:0x34
 *     |                        Reserved                            |:0x38
 *     |   Max_Lat  |   Min_Gnt   | Interrupt Pin | Interrupt Line  |:0x3c
 *
 *
 * if bit 7 in Header Type is 1, device is multifunction.
 */

#define MKPCI(b,d,f) ((((b) & 0xff) << 16)  | \
                      (((d) & 0x1f)  << 11) |  \
                      (((f) &  0x7)  <<  8))
#define PCIBUS(bdf) (((bdf) >> 16) & 0xff)
#define PCIDEV(bdf) (((bdf) >> 11) & 0x1f)
#define PCIFUN(bdf) (((bdf) >>  8) &  0x7)

#define PCI_ADDRESS 0xcf8
#define PCI_DATA    0xcfc

struct PCIDev {
   struct PCIDev *next;

   uint32 bdf;
   uint16 vendor;
   uint16 device;

   uint16 command;
   uint16 status;
   uint8 revision;
   uint8 baseClass;
   uint8 subClass;
   uint8 regInterface;

   uint8 cacheLine;
   uint8 latTimer;
   uint8 headerType;
   uint8 bist;
   uint16 subsystemVendor;
   uint16 subsystemDevice;

   uint32 BAR[6];
   uint32 size[6];

   uint32 ROMbase;
   uint8 irqPin;
   uint8 irqLine;
   uint16 _pad;
} __attribute__((packed));


volatile uint64 pciAddress; // vprobes
volatile uint64 pciData;    // vprobes

struct PCIDev *pciRoot;

enum PCIRW {
   PCI_READ,
   PCI_WRITE
};


uint32
PCICfgRW(uint32 address, uint32 reg, uint32 data, enum PCIRW rw)
{
   if (PCIBUS(address)) {
      address |= 1; /* type 1 address */
   }

   address |= 0x80000000 | reg;

   outl(address, PCI_ADDRESS);

   if (rw == PCI_READ) {
      data = inl(PCI_DATA);
   } else {
      outl(data, PCI_DATA);
   }

   return data;
}


#define IOBAR(b) ((b) & 1)

static inline Bool
HasBAR(uint8 base)
{
   return !((base == 0x0) || /* legacy */
            (base == 0x5) || /* memory controller */
            (base == 0x6) || /* bridge */
            (base > 0x11));  /* reserved */
}

uint32
GetBARSize(uint32 bdf, uint32 reg)
{
   uint32 bar, size;

   bar = PCICfgRW(bdf, reg, 0, PCI_READ);
   PCICfgRW(bdf, reg, 0xffffffff, PCI_WRITE);
   size = PCICfgRW(bdf, reg, 0, PCI_READ);
   PCICfgRW(bdf, reg, bar, PCI_WRITE);

   if (IOBAR(bar)) {
      size = (~(size & ~0x1UL) + 1) & 0xffff;
   } else {
      size = ~(size & ~0xfUL) + 1;
   }

   return size;
}


void
EnumeratePCI(void)
{
   uint32 bus, device, fcn, data;
   struct PCIDev *last = NULL;

   for (bus = 0; bus < 8; bus++) {
      for (device = 0; device < 32; device++) {
         uint32 maxFunction = 1;
         Bool bridge = FALSE;

         data = PCICfgRW(MKPCI(bus, device, 0), 0, 0, PCI_READ);

         if ((data & 0xffff) == 0xffff) {
            continue;
         }

         /* Check for multifunction */
         data = PCICfgRW(MKPCI(bus, device, 0), 0x0c, 0, PCI_READ);

         if (((data >> 16) & 0xff) & 0x80) {
            maxFunction = 8;
         }

         if (((data >> 16) & 0x7f) == 1) {
            bridge = TRUE;
         }


         for (fcn = 0; fcn < maxFunction; fcn++) {
            struct PCIDev *new;
            uint32 k, reg, bdf = MKPCI(bus, device, fcn);

            data = PCICfgRW(bdf, 0, 0, PCI_READ);

            if ((data & 0xffff) == 0xffff) {
               continue;
            }

            pciAddress = bdf;
            pciData = data;
            asm(".global HavePCI\nHavePCI:\n");

            new = alloc(sizeof(struct PCIDev));

            if (pciRoot == NULL) {
               pciRoot = new;
            } else {
               last->next = new;
            }
            last = new;

            new->next = NULL;
            new->bdf = bdf;
            new->vendor = data & 0xffff;
            new->device = data >> 16;

            data = PCICfgRW(bdf, 0x04, 0, PCI_READ);
            new->command = data & 0xffff;
            new->status = data >> 16;

            data = PCICfgRW(bdf, 0x08, 0, PCI_READ);
            new->revision = data & 0xff;
            new->baseClass = (data >> 24) & 0xff;
            new->subClass = (data >> 16) & 0xff;
            new->regInterface = (data >> 8) & 0xff;

            data = PCICfgRW(bdf, 0x0c, 0, PCI_READ);
            new->cacheLine = data & 0xff;
            new->latTimer = (data >> 8) & 0xff;
            new->headerType = (data >> 16) & 0xff;
            new->bist = (data >> 24) & 0xff;

            data = PCICfgRW(bdf, 0x2c, 0, PCI_READ);
            new->subsystemVendor = data & 0xffff;
            new->subsystemDevice = data >> 16;

            data = PCICfgRW(bdf, 0x3c, 0, PCI_READ);
            new->irqPin = data & 0xff;
            new->irqLine = (data >> 8) & 0xff;

            TEST_VPROBE(new->irqPin, new->irqLine);

            new->ROMbase = PCICfgRW(bdf, 0x30, 0, PCI_READ);

            if (HasBAR(new->baseClass)) {
               for (k = 0, reg = 0x10; reg <= 0x24; k++, reg += 4) {
                  new->BAR[k] = PCICfgRW(bdf, reg, 0, PCI_READ);
                  new->size[k] = (new->BAR[k] == 0) ? 0 : GetBARSize(bdf, reg);
               }
            }
         }
      }
   }
}


/* void */
/* EnumeratePCIBridges(void) */
/* { */

/* } */


/*
 * PCI IRQ Routing Table Specification can be found at
 * http://www.microsoft.com/whdc/archive/pciirq.mspx
 *
 * The PIR is probably outdated and the ACPI table should probably be
 * used instead.
 *
 */


#define PIRBASE  0x0f0000
#define PIRLIMIT 0x100000
/* "$PIR" */
#define PIR      0x52495024

struct PciIrqRoutingSlot {
   uint8 bus;       /* PCI bus number */
   uint8 device;    /* PCI device number (upper 5 bits) */
   uint8 intALink;  /* Link value for INTA# */
   uint16 intABits; /* IRQ bitmap for INTA# */
   uint8 intBLink;  /* Link value for INTB# */
   uint16 intBBits; /* IRQ bitmap for INTB# */
   uint8 intCLink;  /* Link value for INTC# */
   uint16 intCBits; /* IRQ bitmap for INTC# */
   uint8 intDLink;  /* Link value for INTD# */
   uint16 intDBits; /* IRQ bitmap for INTD# */
   uint8 slot;      /* slot number */
   uint8 reserved;
} __attribute__((packed));

struct PciIrqRouting {
   uint32 sig;      /* signature */
   uint16 version;  /* version (1.0) */
   uint16 size;     /* table size */
   uint8 bus;       /* PCI interrupt router's bus */
   uint8 devFunc;   /* PCI interrupt router's device and function (5/3) */
   uint16 xirq;     /* PCI exclusive IRQs (PCI only IRQ bitmap) */
   uint32 compat;   /* Compatible PCI interrupt router (vendor/device ID)*/
   uint32 miniport; /* Data passed to IRQ miniport's Initialize() function */
   uint8 reserved[11];
   uint8 checksum;  /* sum(table) % 256 == 0 (including this checksum) */
   struct PciIrqRoutingSlot entries[0];
} __attribute__((packed));


struct PciIrqRouting *routingTable; // vprobes

void
RouteIRQs(void)
{
   struct PciIrqRouting *table;
   VA m;

   /*
    * Search for the IRQ routing table (the region is already identity mapped).
    */
   for (m = PIRBASE; m < PIRLIMIT; m += 16) {
      if (*(uint32 *)m == PIR) {
         table = (struct PciIrqRouting *)m;
         break;
      }
   }

   if (m >= PIRLIMIT) {
      return;
   }

   routingTable = table;
   asm(".global RouteIRQ\nRouteIRQ:\n");
}


void
PCIMMap(void)
{

}


void
MapPCI(void)
{
   pciRoot = NULL;

   EnumeratePCI();
/*    EnumeratePCIBridges(); */
   RouteIRQs();

   /* Map memory used by PCI devices (have to remap the framebuffer). */
   PCIMMap();
}
