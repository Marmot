/*
 * panic.c --
 *
 *      Panic - print a message to the screen and bail.
 */

#include <marmot.h>

static char *types[] = {
   "Assertion failed:",
   "Unimplemented feature:",
   "Miscellaneous failure:"
};


/*
 * formatDec --
 *
 *      Limited decimal number formatter that only handles unsigned
 *      numbers less than 10,000,000.
 */

void
formatDec(char s[], uint64 n)
{
   uint64 p, k;

   if (n >= 10000000) {
      return;
   }

   for (p = 0; n; p++) {
      s[p] = '0' + n % 10;
      n /= 10;
   }

   for (k = 0; k < p / 2; k++) {
      char c;
      c = s[k];
      s[k] = s[p - k - 1];
      s[p - k - 1] = c;
   }
}

void
Bug(uint64 type, char *cond, char *file, uint64 line)
{
   Point p0, p1;
   uint64 maxStr = ((uint64)xResolution - 8) / 8;
   char lineStr[] = {'L', 'i', 'n', 'e', ':', ' ', 0, 0, 0, 0, 0, 0, 0, 0};

   /*
    * Bug message looks like:
    *
    * Type message:
    * cond
    * file
    * Line line
    *
    * Type message can be:
    *
    *      BUG_ASSERT Assertion failed
    *      BUG_UNIMPL Unimplemented feature
    *      BUG_MISC   Miscellaneous failure
    *
    */

   p0.x = 0;
   p0.y = 0;
   p1.x = 1280;
   p1.y = 4 * 16 + 5 * 4;
   ColorRectangle(COLOR_RED, p0, p1);

   p0.x = 4;
   p0.y = 4;
   PrintMessage(COLOR_BLACK, p0, types[type]);

   p0.y += 20;
   if (strlen(cond) > maxStr) {
      cond[maxStr - 1] = '\0';
   }
   PrintMessage(COLOR_BLACK, p0, cond);

   p0.y += 20;
   if (strlen(file) > maxStr) {
      file[maxStr - 1] = '\0';
   }
   PrintMessage(COLOR_BLACK, p0, file);

   p0.y += 20;
   formatDec(lineStr + 6, line);
   PrintMessage(COLOR_BLACK, p0, lineStr);

   cli();
   asm("hlt");
}
