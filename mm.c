/*
 * mm.c --
 *
 *      Base memory management functions.
 *
 */

/* XXX: this file's a mess */

#include <marmot.h>


#define PHYSMEM_BITMAP_BASE (STACKTOP)

#define MAX_E820 64
#define E820_RAM       1
#define E820_RSVD      2
#define E820_ACPI_DATA 3
#define E820_ACPI_NVS  4


typedef enum {
   MEMTYPE_RAM,
   MEMTYPE_ROM,
   MEMTYPE_ACPI_DATA,
   MEMTYPE_ACPI_NVS,
   MEMTYPE_IO,
   MEMTYPE_HOLE
} MemRegionType;

struct e820 {
   uint64 base;
   uint64 length;
   uint32 type;
} __attribute__((packed));

extern struct e820 e820_map[MAX_E820];
extern uint64 e820_entries;

typedef uint64 PML4E;
typedef uint64 PDPE;
typedef uint64 PDE;
typedef uint64 PTE;


#define PAGE_ADDR_MASK  0x000ffffffffff000

#define PML4E_ADDR_MASK 0x000ffffffffff000
#define PML4E_NX  (1 << 63)
#define PML4E_A   (1 << 5)
#define PML4E_PCD (1 << 4)
#define PML4E_PWT (1 << 3)
#define PML4E_US  (1 << 2)
#define PML4E_RW  (1 << 1)
#define PML4E_P   (1 << 0)

#define PDPE_ADDR_MASK 0x000ffffffffff000
#define PDPE_NX  (1 << 63)
#define PDPE_A   (1 << 5)
#define PDPE_PCD (1 << 4)
#define PDPE_PWT (1 << 3)
#define PDPE_US  (1 << 2)
#define PDPE_RW  (1 << 1)
#define PDPE_P   (1 << 0)

#define PDE_ADDR_MASK 0x000ffffffffff000
#define PDE_NX  (1 << 63)
#define PDE_A   (1 << 5)
#define PDE_PCD (1 << 4)
#define PDE_PWT (1 << 3)
#define PDE_US  (1 << 2)
#define PDE_RW  (1 << 1)
#define PDE_P   (1 << 0)

#define PTE_ADDR_MASK 0x000ffffffffff000
#define PTE_NX  (1 << 63)
#define PTE_G   (1 << 8)
#define PTE_PAT (1 << 7)
#define PTE_D   (1 << 6)
#define PTE_A   (1 << 5)
#define PTE_PCD (1 << 4)
#define PTE_PWT (1 << 3)
#define PTE_US  (1 << 2)
#define PTE_RW  (1 << 1)
#define PTE_P   (1 << 0)


#define PTE_ENTRIES (PAGE_SIZE / sizeof(PTE))


#define PTOFFSET(va) (((va) >> 12) & 0x1ff)

#define MKPML4E(pdp, flags) (((pdp) & PML4E_ADDR_MASK) | flags)
#define MKPDPE(pd, flags)   (((pd) & PDPE_ADDR_MASK) | flags)
#define MKPDE(pt, flags)    (((pt) & PDE_ADDR_MASK) | flags)
#define MKPTE(pa, flags)    (((pa) & PTE_ADDR_MASK) | flags)


/*
 * Use the AVL bits (11-9) in each PTE to mark a page as used or free.
 */

#define MM_STATUS_MASK  0x0000000000000e00
#define MM_PAGE_USED    0x200
#define MM_PAGE_INVALID 0x400

#define CANONICAL_MASK     0x00007fffffffffff


#define BITMAP_SIZE(count) (count >> 3)

typedef uint8 MemoryPool;

typedef struct {
   PA base;
   PA limit;
   MemRegionType type;
} MemRegion;


#define MEMORY_REGIONS_MAX 64

static MemRegion memoryRegions[MEMORY_REGIONS_MAX];
static uint64 memoryRegionCount;

typedef struct {
   char *name;
   PA base;
   PA limit;
   uint64 count;
   uint8 *map;
} Pool;

Pool pools[4];

static char *poolNames[] = {
   "ident",
   "kernel",
   "priv",
   "user"
};



MemRegionType __init
GetMemType(PA addr)
{
   uint64 r;

   for (r = 0; r < memoryRegionCount; r++) {
      if (addr >= memoryRegions[r].base && addr < memoryRegions[r].limit) {
         return memoryRegions[r].type;
      }
   }

   return MEMTYPE_HOLE;
}


void __init
AddMemoryRegion(PA base, PA limit, MemRegionType type)
{
   if (memoryRegionCount == MEMORY_REGIONS_MAX) {
      // PANIC();
   }

   memoryRegions[memoryRegionCount].base = base;
   memoryRegions[memoryRegionCount].limit = limit;
   memoryRegions[memoryRegionCount].type = type;

   memoryRegionCount++;
}


/*
 * FixupE820 --
 *
 *      Sort through the e820 entries, ensuring they're in order, and
 *      joining adjacent identical entries.  Then add the regions to
 *      the region map.
 *
 */

void __init
FixupE820(void)
{
   uint64 d, j, k;
   int doneSorting;

   if (e820_entries < 2) {
      return;
   }

   /* sort the entries */
   doneSorting = 0;
   while (!doneSorting) {
      doneSorting = 1;

      for (k = 0; k < e820_entries - 1; k++) {
         if (e820_map[k].base > e820_map[k + 1].base) {
            struct e820 tmp;

            tmp.base = e820_map[k].base;
            tmp.length = e820_map[k].length;
            tmp.type = e820_map[k].type;

            e820_map[k].base = e820_map[k + 1].base;
            e820_map[k].length = e820_map[k + 1].length;
            e820_map[k].type = e820_map[k + 1].type;

            e820_map[k].base = tmp.base;
            e820_map[k].length = tmp.length;
            e820_map[k].type = tmp.type;

            doneSorting = 0;
         }
      }
   }

   /* merge adjacent entries */
   k = 0;
   j = 1;
   while (k < e820_entries - 1) {
      if (((e820_map[k].base + e820_map[k].length) >= e820_map[j].base) &&
          (e820_map[k].type == e820_map[j].type)) {

         /* merge j into k */
         if (e820_map[k].base + e820_map[k].length <
             e820_map[j].base + e820_map[j].length) {
            /* second entry has higher limit than first */
            e820_map[k].length = ((e820_map[j].base + e820_map[j].length) -
                                  e820_map[k].base);
         } else {
            /* first entry entirely overlaps second - do nothing */
         }

         /* move rest of entries down */
         for (d = k + 1; j < e820_entries; j++) {
            e820_map[d].base = e820_map[j].base;
            e820_map[d].length = e820_map[j].length;
            e820_map[d].type = e820_map[j].type;
         }

         e820_entries--;
      } else {
         k++;
      }

      j = k + 1;
   }

   /* adjust to page boundaries */
   for (k = 0; k < e820_entries; k++) {

      if (e820_map[k].base & 0xfff) {
         if (e820_map[k].type == E820_RAM) {
            /* RAM - adjust base up */
            e820_map[k].length -= e820_map[k].base & 0xfff;
            e820_map[k].base = (e820_map[k].base & (~0xfffULL)) + 4096;
         } else {
            /* otherwise adjust down*/
            e820_map[k].length += e820_map[k].base & 0xfff;
            e820_map[k].base = e820_map[k].base & (~0xfffULL);
         }
      }

      if (((e820_map[k].base + e820_map[k].length) & 0xfff) &&
          e820_map[k].type == E820_RAM) {
         /* adjust limit down */
         e820_map[k].length -= (e820_map[k].base + e820_map[k].length) & 0xfff;
      }
   }
}


void __init
AddE820Regions(void)
{
   uint64 k;

   FixupE820();

   for (k = 0; k < e820_entries; k++) {
      MemRegionType t = MEMTYPE_HOLE;

      switch (e820_map[k].type) {
      case E820_RAM:
         t = MEMTYPE_RAM;
         break;
      case E820_RSVD:
         t = MEMTYPE_ROM;
         break;
      case E820_ACPI_DATA:
         t = MEMTYPE_ACPI_DATA;
         break;
      case E820_ACPI_NVS:
         t = MEMTYPE_ACPI_NVS;
         break;
      }

      AddMemoryRegion((PA)e820_map[k].base,
                      (PA)(e820_map[k].base + e820_map[k].length),
                      t);
   }
}


extern uint64 GetCR3(void);
asm(".global GetCR3\n"
    "GetCR3:\n"
    "\tmovq    %cr3, %rax\n"
    "\tret\n");
extern uint64 GetCR2(void);
asm(".global GetCR2\n"
    "GetCR2:\n"
    "\tmovq    %cr2, %rax\n"
    "\tret\n");
extern void SetCR3(uint64);
asm(".global SetCR3\n"
    "SetCR3:\n"
    "\tmovq    %rdi, %cr3\n"
    "\tret\n");
extern void SetCR2(uint64);
asm(".global SetCR2\n"
    "SetCR2:\n"
    "\tmovq    %rdi, %cr2\n"
    "\tret\n");
extern void FlushCR3(void);
asm(".global FlushCR3\n"
    "FlushCR3:\n"
    "\tmovq    %cr3, %rax\n"
    "\tmovq    %rax, %cr3\n"
    "\tret\n");


/* global flag that is TRUE when swapping is enabled */
volatile Bool swapping = FALSE;

// XXX: for now
#define PAGED(e) FALSE

/*
 * WalkPT --
 *
 *      Walk a page table for a given VA and return a pointer to the
 *      PTE.  If no entry exists for the virtual address in any of the
 *      tables, the entry is created.
 *
 */

PTE *
WalkPT(VA addr)
{
   PML4E *pml4, *pml4e;
   PDPE *pdp, *pdpe;
   PDE *pd, *pde;
   PTE *pt, *pte;

   /* PML4 => PDP */

   pml4 = (PML4E *)(GetCR3() & ~0xfffULL);
   pml4e = &pml4[((addr >> 39) & 0x1ff)];

   if (*pml4e & PML4E_P) {
      pdp = (PDPE *)(*pml4e & PML4E_ADDR_MASK);
   } else {
      /* PDP is swapped out or not yet allocated. */

      if (swapping && PAGED(*pdpe)) {
         /* Swap it in. */

      } else {
         /* Allocate new PDP */
         pdp = (PDPE *)PageAlloc(MM_VA_IDENT, 0);

         bzero(pdp, PAGE_SIZE);
         *pml4e = MKPDPE((PA)pdp, PDPE_P);
      }
   }

   /* PDP => PD */

   pdpe = &pdp[((addr >> 30) & 0x1ff)];

   if (*pdpe & PDPE_P) {
      pd = (PDE *)(*pdpe & PDPE_ADDR_MASK);
   } else {
      /* PD is  either paged out or not yet allocated. */

      if (swapping && PAGED(*pdpe)) {
         /* Swap it in. */

      } else {
         /* Allocate new PD */

         pd = (PDE *)PageAlloc(MM_VA_IDENT, 0);
         bzero(pd, PAGE_SIZE);
         *pdpe = MKPDPE((PA)pd, PDPE_P);
      }
   }


   pde = &pd[((addr >> 21) & 0x1ff)];

   if (*pde & PDE_P) {
      pt = (PTE *)(*pde & PDE_ADDR_MASK);
   } else {
      if (swapping && PAGED(*pde)) {
         /* Swap it in */

      } else {
         /* Allocate new PT */

         pt = (PTE *)PageAlloc(MM_VA_IDENT, 0);
         bzero(pt, PAGE_SIZE);
         *pde = MKPDE((PA)pt, PDE_P);
      }
   }

   pte = &pt[((addr >> 12) & 0x1ff)];

   return pte;
}



extern uint64 GetCPL(void);
asm(".global GetCPL\n"
    "GetCPL:\n"
    "\tmovl    %cs, %eax\n"
    "\tandl    $3, %eax\n"
    "\tmovzwq  %ax, %rax\n" /* may not be necessary */
    "\tret\n");

/*
 * GetFreePA --
 *
 *      Find a free physical page from the pool and return its
 *      address.  Once swapping is turned on, this function will
 *      always return a free page.
 */

PA
GetFreePA(MemoryPool pool)
{
   PA freePage = MM_PA_INVALID;
   uint64 byte, bit;
   uint64 pageOffset = -1ULL;
   uint64 r;

   /*
    * Find a 0 bit in the bitmask and convert it to a page offset.
    */

   for (byte = 0; byte < BITMAP_SIZE(pools[pool].count); byte++) {
      if (pools[pool].map[byte] != 0xff) {

         for (bit = 0; bit < 8; bit++) {
            if (pools[pool].map[byte] & (1 << bit)) {
               continue;
            }

            break;
         }

         pageOffset = byte * 8 + bit;
         pools[pool].map[byte] |= (1 << bit);

         break;
      }
   }

   if (pageOffset == -1ULL) {
      // XXX: do some paging instead of just shutting down
      asm("cli; hlt");
   }

   /*
    * Now scan through the regions, finding where this page is in memory.
    */

   for (r = 0; r < memoryRegionCount; r++) {
      if (memoryRegions[r].type == MEMTYPE_RAM &&
          pools[pool].base >= memoryRegions[r].base &&
          pools[pool].base < memoryRegions[r].limit) {

         if (memoryRegions[r].base + (pageOffset << 12) <
             memoryRegions[r].limit) {

            freePage = memoryRegions[r].base + (pageOffset << 12);
            break;

         } else {
            pageOffset -= (memoryRegions[r].limit -
                           memoryRegions[r].base) >> 12;
         }
      }
   }

/*    tval = pageOffset; tval2 = freePage; asm("\t.global test\ntest:\n"); */

   return freePage;
}

void
ReleasePA(PA addr)
{
   /* XXX */
}


void
AdjustVPF(VA *pDesired, MemoryPool *pPool, uint64 *pFlags)
{
   VA desired;
   MemoryPool pool;
   uint64 flags;

   desired = *pDesired;
   pool = *pPool;
   flags = *pFlags;

   switch (GetCPL()) {
   case 0:
      pool = POOL_KERNEL;

      if (desired == MM_VA_DONT_CARE) {
         desired = MM_VA_KERNEL_START;
      } else if (desired == MM_VA_HEAP) {
         desired = MM_VA_KERNEL_HEAP;
      } else if (desired == MM_VA_IDENT) {
         desired = MM_VA_LOADER_START;
         pool = POOL_IDENT;
      }
      break;
   case 1:
   case 2:
      if (desired == MM_VA_DONT_CARE) {
         desired = MM_VA_PRIV_START;
      } else if (desired == MM_VA_HEAP) {
         desired = MM_VA_PRIV_HEAP;
      }
      pool = POOL_PRIVILEGED;
      break;
   case 3:
      if (desired == MM_VA_DONT_CARE) {
         desired = MM_VA_USER_START;
      } else if (desired == MM_VA_HEAP) {
         desired = MM_VA_USER_HEAP;
      }
      desired = MM_VA_USER_START;
      pool = POOL_USER;
      flags |= PTE_US;
      break;
   }

   desired &= PTE_ADDR_MASK; // align to page boundary and ensure canonicality

   *pDesired = desired;
   *pPool = pool;
   *pFlags = flags;
}


/*
 * PageAlloc --
 *
 *      Allocate a single page of memory and a physical page to back
 *      it.  If a virtual address is requested, the allocater attempts
 *      to map there.  If MM_VA_DONT_CARE is passed in instead, the
 *      allocater will map at the first available address.
 */

VA
PageAlloc(VA desired, uint64 flags)
{
   PA freePage;
   PTE *pte = NULL;
   VA va = MM_VA_INVALID;
   MemoryPool pool = POOL_USER;

   AdjustVPF(&desired, &pool, &flags);

   freePage = GetFreePA(pool);

   if (pool == POOL_IDENT) {
      pte = WalkPT((VA)freePage);
      ASSERT((*pte & MM_STATUS_MASK) == 0);

      va = (VA)freePage;
   } else {
      VA search = desired;

      /*
       * Scan for an unused VA.  If MM_VA_DONT_CARE was passed in,
       * this may be slow...
       */

      pte = WalkPT(desired);

      while (*pte & MM_STATUS_MASK) {
         search += PAGE_SIZE;
         pte = WalkPT(search);
      }

      va = search;
   }

   /* Update PTE to point to freePage */
   *pte = MKPTE(freePage, flags | MM_PAGE_USED | PTE_P);

   return va;
}


/*
 * RegionAlloc --
 *
 *      Allocate a contiguous region of virtual memory.  Pages cannot
 *      be allocated from POOL_IDENT here.
 *
 *      Returns NULL if a contiguous region cannot be found.
 *
 *      This is intended to be the general-purpose memory allocater.
 */

VA
RegionAlloc(VA desired, uint64 nPages, uint64 flags)
{
   MemoryPool pool = POOL_USER;
   VA found = MM_VA_INVALID, start, scan, limit;
   PTE *pte;
   uint64 n;

   AdjustVPF(&desired, &pool, &flags);

   ASSERT(pool != POOL_IDENT);

   if (desired < MM_VA_PRIV_START) {
      limit = MM_VA_PRIV_START;
   } else if (desired < MM_VA_USER_START) {
      limit = MM_VA_USER_START;
   } else {
      limit = MM_VA_CANONICAL_TOP;
   }

   /* Need to find an nPage region in virtual space that is available. */

   for (start = desired; start < limit; start += PAGE_SIZE) {
      pte = WalkPT(start);

      if (*pte & MM_STATUS_MASK) {
         continue;
      }

      for (scan = start + PAGE_SIZE, n = 0;
           n < nPages && scan < limit;
           n++, scan += PAGE_SIZE) {

         pte = WalkPT(scan);

         if (*pte & MM_STATUS_MASK) {
            break;
         }
      }

      if (n == nPages) {
         found = start;
         break;
      }
   }

   if (found == MM_VA_INVALID) {
      return NULL;
   }

   for (scan = found, n = 0; n < nPages; n++, scan += PAGE_SIZE) {
      pte = WalkPT(scan);
      *pte = MKPTE(0, flags | MM_PAGE_USED); /* Physmem allocation is lazy. */
   }

   return found;
}


/*
 * PageFree --
 *
 *      Release a page of memory and its virtual mapping.
 */

void
PageFree(VA page)
{
   PTE *pte;
   PA physPage;

   pte = WalkPT(page);
   physPage = *pte & PTE_ADDR_MASK;

   *pte = 0;
   ReleasePA(physPage);
}


/*
 * PageRemap --
 *
 *      Remap a page's virtual address.  Return TRUE if successful,
 *      and FALSE if the target VA is already in use or is outside the
 *      allowable range.
 */

Bool
PageRemap(VA current, VA new)
{

   return FALSE;
}


#define PF_NP    0x01
#define PF_RW    0x02
#define PF_US    0x04
#define PF_RSVD  0x08
#define PF_ID    0x10

/*
 * HandlePF --
 *
 *      Page fault handler (int 14).  Called from stub in interrupts.S.
 */

void
HandlePF(ExcFrame *f)
{
   VA CR2;
   PA freePage;
   PTE *pte;
   MemoryPool pool = POOL_USER;

   CR2 = GetCR2();
   pte = WalkPT(CR2);

   if (f->errorCode & PF_NP) {
      /* #PF caused by permissions will be handled once tasks are
          implemented. */
      UNIMPLEMENTED("#PF");
   }

   /*
    * #PF caused by mapped but not allocated page - allocate it here.
    */

   switch (f->cs & 0x3) {
   case 0:
      pool = POOL_KERNEL;
      break;
   case 1:
   case 2:
      pool = POOL_PRIVILEGED;
      break;
   case 3:
      pool = POOL_USER;
      break;
   }

   /* XXX: Once swapping is implemented, will need to switch to kernel
    * stack and make this a deferred function call as getting a free
    * PA may take time and require interrupts to be enabled. */

   freePage = GetFreePA(pool);
   *pte = MKPTE(freePage, (*pte & 0xfff) | PTE_P);

   SetCR2(0);
}


/*
 * MapFirstPT --
 *
 *      Identity map the first page table.  This is called very early
 *      in startup and is needed by the memory mapper.
 */

void __init
MapFirstPT(void)
{
   PA current;
   PTE *pt = (PTE *)PTBASE;

   /*
    * PML4/PDPT/PD are already initialized.
    */

   /* First page is BIOS data area - mark ro */
   pt[PTOFFSET(0)] = MKPTE(0, PTE_P | MM_PAGE_INVALID);

   /*
    * Below STACKTOP (0x18000), all pages are used by the loader.
    * Mark them as such.
    */

   for (current = PAGE_SIZE;
        current < PTE_ENTRIES * PAGE_SIZE;
        current += PAGE_SIZE) {
      MemRegionType type;
      PTE pte;

      type = GetMemType(current);

      if (type == MEMTYPE_RAM) {
         pte = MKPTE(current, PTE_P|PTE_RW);

         if (current < STACKTOP) {
            pte |= MM_PAGE_USED;
         }

      } else if (type == MEMTYPE_ROM) {
         pte = MKPTE(current, PTE_P | MM_PAGE_INVALID);
      } else if (type == MEMTYPE_IO ||
                 type == MEMTYPE_ACPI_DATA ||
                 type == MEMTYPE_ACPI_NVS) {
         pte = MKPTE(current, PTE_P|PTE_RW|PTE_PCD | MM_PAGE_INVALID);
      } else {
         pte = MKPTE(current, 0 | MM_PAGE_INVALID); /* mark page NP */
      }

      pt[PTOFFSET(current)] = pte;
   }
}


/*
 * MapIORegion --
 *
 *      Identity map an IO region.
 */

void
MapIORegion(PA start, PA end, char *name)
{
   PTE *pte;
   VA va;

   start &= PAGE_ADDR_MASK;
   end = (PAGE_SIZE - 1 + end) & PAGE_ADDR_MASK;

   for (va = start; va < end; va += PAGE_SIZE) {
      pte = WalkPT(va);
      *pte = MKPTE(va, MM_PAGE_INVALID|PTE_PCD|PTE_RW|PTE_P);
   }
}


/*
 * AddRegionsToPools --
 *
 *      Go through memory regions and memory pools and assign base and
 *      limit addresses to each pool.
 */

void __init
AddRegionsToPools(void)
{
   uint64 r, p, countLeft;
   PA startAddr;

   r = p = 0;
   countLeft = pools[p].count * PAGE_SIZE;
   pools[p].base = startAddr = memoryRegions[0].base;

   while (r < memoryRegionCount && p < 4) {
      if (memoryRegions[r].type != MEMTYPE_RAM) {
         r++;
         continue;
      }

      if (startAddr < memoryRegions[r].base) {
         /* Update startAddr to the current region. */
         startAddr = memoryRegions[r].base;
      }

      if (countLeft == 0) {
         countLeft = pools[p].count * PAGE_SIZE;
         pools[p].base = startAddr;
      }

      if (startAddr + countLeft <= memoryRegions[r].limit) {
         startAddr += countLeft;
         countLeft = 0;
         pools[p].limit = startAddr; /* actually end address here */
         p++;
      } else if (startAddr + countLeft > memoryRegions[r].limit) {
         countLeft -= memoryRegions[r].limit - startAddr;
         r++;
      }
   }
}


static uint64 __init
CalculateTotalMem(void)
{
   uint64 mem, r;

   for (mem = 0, r = 0; r < memoryRegionCount; r++) {
      if (memoryRegions[r].type == MEMTYPE_RAM) {
         mem += memoryRegions[r].limit - memoryRegions[r].base;
      }
   }

   return mem;
}


/*
 * CreateMemPools --
 *
 *      Divide available physical memory into pools.
 *
 *      Initial allocations are:
 *
 *         ident  - 8MB  - used for page tables and other basic data structures
 *         kernel - 8MB  - kernel text/data
 *         priv   - 16MB - privileged processes (drivers)
 *         user   - rest - user physmem allocation
 */

#define MB (1024ULL * 1024ULL)
#define GB (1024ULL * 1024ULL * 1024ULL)

void __init
CreateMemPools(void)
{
   uint64 totalMem, totalPages, bitmapPages, p;
   PA addr;

   /* the number of pages in each pool must be divisible by 8 */
   pools[0].name = poolNames[0];
   pools[0].count = 8 * MB / PAGE_SIZE;
   pools[1].name = poolNames[1];
   pools[1].count = 8 * MB / PAGE_SIZE;
   pools[2].name = poolNames[2];
   pools[2].count = 16 * MB / PAGE_SIZE;
   pools[3].name = poolNames[3];

   /*
    * Each page of bitmask can represent 32768 pages (128MB).  As the
    * range 0x18000 - 0x98000 is used for bitmasks, this allows a
    * maximum of 4194304 pages or 16GB physical memory.  Should this
    * limit become onerous, this should be pretty easy to revisit.
    */

   totalMem = CalculateTotalMem();

   if (totalMem > 16 * GB) {
      totalMem = 16 * GB;
   }

   if (totalMem < 32 * MB) {
      // XXX: PANIC("Not enough memory");
      asm("cli; hlt\n");
   } else if (totalMem < 64 * MB) {
      /* Small mem - halve pool allocations. */
      pools[0].count = 4 * MB / PAGE_SIZE;
      pools[1].count = 4 * MB / PAGE_SIZE;
      pools[2].count = 8 * MB / PAGE_SIZE;
   }

   pools[3].count = (totalMem / PAGE_SIZE - pools[0].count -
                     pools[1].count - pools[2].count);

   totalPages = (pools[0].count + pools[1].count +
                 pools[2].count + pools[3].count);

   /* round up to next full bitmap page */
   if (totalPages & 0x7fff) {
      totalPages = (totalPages & 0xffffffffffff8000) + 0x8000;
   }

   bitmapPages = totalPages >> 15; /* div 32768 */

   //tval = bitmapPages; asm("\t.global test\ntest:\n");

   pools[0].map = (uint8 *)PHYSMEM_BITMAP_BASE;
   pools[1].map = (uint8 *)((uint64)pools[0].map + BITMAP_SIZE(pools[0].count));
   pools[2].map = (uint8 *)((uint64)pools[1].map + BITMAP_SIZE(pools[1].count));
   pools[3].map = (uint8 *)((uint64)pools[2].map + BITMAP_SIZE(pools[2].count));

   AddRegionsToPools();

   /*
    * Finally mark known pages as used in the ident bitmap.
    */

   /* zero out bitmaps */
   bzero(pools[0].map, BITMAP_SIZE(pools[0].count));
   bzero(pools[1].map, BITMAP_SIZE(pools[1].count));
   bzero(pools[2].map, BITMAP_SIZE(pools[2].count));
   bzero(pools[3].map, BITMAP_SIZE(pools[3].count));

   /*
    * Used pages are:
    *            0 -  0x1000 : BDA
    *       0x1000 -  0x6000 : GDT/IDT/PML4/PDPT/PD/PT
    *       0x6000 -  0x8000 : loader bss (can be freed later)
    *       0x8000 - 0x10000 : loader text/data/rodata
    *      0x10000 - 0x18000 : stack
    *      0x18000 - ?       : bitmaps
    */

   for (addr = 0; addr < STACKTOP; addr += PAGE_SIZE) {
      uint64 ppn, byte, bit;

      ppn = addr >> 12;

      byte = ppn >> 3;
      bit = ppn & 7;

      pools[0].map[byte] |= (uint8)(1 << bit);
   }

   for (p = 0; p < bitmapPages; p++) {
      uint64 addr, ppn, byte, bit;

      addr = (PA)PHYSMEM_BITMAP_BASE + p * PAGE_SIZE;
      ppn = addr >> 12;

      byte = ppn >> 3;
      bit = ppn & 7;

      pools[0].map[byte] |= (uint8)(1 << bit);
   }
}



void __init
MapMemory(void)
{
   /*
    * Add known memory regions to list.
    */

   memoryRegionCount = 0;
   AddE820Regions();

   /*
    * Fill in the first page table - that will provide some breathing
    * room to set up all the various data structures.  As part of
    * this, clobber the original page tables (though since this region
    * will be identity mapped, it won't make a difference).
    */

   MapFirstPT();
   FlushCR3();

   asm("\t.global mapped\nmapped:\n");

   /* Now there's room for the rest of the page tables. */
   
   CreateMemPools();
}
